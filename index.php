<?php
namespace sw;

header('Cache-Control: public');
?><html><head>
<title>Package SwLib [SW PHP Library]</title>
<style type="text/css">
<!--
body, p, div, h1, h2, tr, td, th, pre {
    font-family: monospace;
    font-size: 10pt;
}

body {
    padding: 20px;
}

h1 {
    font-size: 15pt;
}

h2 {
    font-size: 13pt;
}

table {
    margin: 0px;
    padding: 0px;
    border-collapse: collapse;
    width: 100%;
}

tr {
    margin: 0px;
    padding: 0px;
}

td, th {
    margin: 0px;
    padding: 5px;
    border: solid 1px black;
    vertical-align: top;
    text-align: left;
}

th {
    background-color: #eeeeee;
}

col.main-key {
    width: 150px;
}

div.file {
    border: solid 1px black;
    background: #eeeeee;
    margin-bottom: 10px;
    padding: 5px;
}

div.file div.file-name {
    margin: 5px 0px 5px 0px;
    font-weight: bold;
    font-style: italic;
}

div.file table.class-def {
    padding: 0px;
    background-color: #ffffff;
    margin-bottom: 5px;
}

div.file table.class-def td,
div.file table.class-def th {
    background: transparent;
    padding: 1px 5px;
    background-color: #ffffff;
}

div.file table.class-def col.tag {
    width: 100px;
}

span.class-inline-details {
    color: #888888;
    font-weight: normal;
}

-->
</style>
</head><body>
<h1>Package SwLib [SW PHP Library]</h1>
Version: <i><?php include('version'); ?></i><br/>
Author: Stefan Wilhelm<br/>
License: GPL<br/><br/>
For details please refer to &lt;<a href="http://www.atwillys.de/repository/swlib/">http://www.atwillys.de/repository/swlib/</a>&gt;.
</body></html>