<?php

/**
 * Exceptions thrown by class GitProject
 *
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2005-2010
 * @license GPL
 * @version 1.0
 */

namespace sw;

class GitProjectException extends LException {
  
}
