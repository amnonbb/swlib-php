<?php

/**
 * Session management wrapper. This class can be used to replace the PHP standard
 * session handling. Principally this class does nothing differemt, but the class
 * can be easily replaced with a database-based session handling or the like.
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2005-2010
 * @license GPL
 * @version 1.0
 */

namespace sw;

class Session {

  /**
   * The one any only session object
   * @staticvar session
   */
  private static $instance = null;

  /**
   * Session startup, autimatically creates session instance.
   * @param string $name=null
   * @return Session
   */
  public static final function start($name=null) {
    if (is_null(self::$instance)) {
      self::$instance = new self($name);
    }
    return self::$instance;
  }

  /**
   * Closes the session, writes the session file
   */
  public static final function stop() {
    self::$instance = null;
  }

  /**
   * Returns the Session singleton instance
   * @return Session
   */
  public static final function instance() {
    return self::$instance;
  }

  /**
   * Session instance constructor.
   * Starts new session.
   * @param string $name=null
   * @return Session
   */
  private final function __construct($name=null) {
    $name = trim($name);
    if (!empty($name)) {
      if (!is_numeric($name) && !is_alnum($name)) {
        @session_name($name);
      } else {
        throw new LException('Session name is invalid: :name', array(':name' => $name));
      }
    }
    @session_start();
  }

  /**
   * Session instance destructor.
   */
  public final function __destruct() {
    @session_write_close();
  }

  /**
   * Session stop with cockie, vars and cache.
   * @return void
   */
  public final function destroy() {
    $_SESSION = array();
    if (isset($_COOKIE[session_name()])) {
      @setcookie(session_name(), '', time() - 42000, '/');
    }
    @session_unset();
    @session_destroy();
  }

  /**
   * Returns session IDs
   * @return string
   */
  public final function id() {
    return @session_id();
  }

  /**
   * Generates a new session ID, keeps session data
   */
  public final function regrnerateId() {
    if (!@session_regenerate_id()) {
      throw new LException("ID regeneration failed.");
    }
  }

  /**
   * Returns the session name
   * @return string
   */
  public final function getName() {
    return @session_name();
  }

  /**
   * Returns the save path for session variables
   * @return string
   */
  public final function getSavePath() {
    return @session_save_path();
  }

  /**
   * Returns the session cache expire time in
   * minutes.
   * @return int
   */
  public final function getCacheExpireTime() {
    return @session_cache_expire();
  }

  /**
   * Returns an associative array with all saved and not garbaged sessions,
   * where the keys are the session ids and the values are the last file
   * modification time.
   * @return array
   */
  public static final function getActiveSessions() {
    $sessions = array();
    foreach (glob(session_save_path() . "sess_*") as $key => $file) {
      $f = pathinfo($file);
      $f = str_replace('sess_', '', $f['filename']);
      $sessions[$f] = filemtime($file);
    }
    return $sessions;
  }

  /**
   * Returns the IP address of the client browser
   * @return string
   */
  public static final function getClientIpAddress() {
    if (isset($_SERVER["REMOTE_ADDR"]))
      return $_SERVER["REMOTE_ADDR"];
    if (isset($_SERVER["HTTP_CLIENT_IP"]))
      return $_SERVER["HTTP_CLIENT_IP"];
    if (@getenv("HTTP_CLIENT_IP"))
      return @getenv("HTTP_CLIENT_IP");
    if (getenv("HTTP_X_FORWARDED_FOR"))
      return @getenv("HTTP_X_FORWARDED_FOR");
    if (@getenv("REMOTE_ADDR"))
      return @getenv("REMOTE_ADDR");
    return null;
  }

  /**
   *
   */
  public static final function getClientUserAgent() {
    $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
    foreach (array(
      '/chrome/i' => 'chrome', // chrome contains the text "safari"
      '/safari/i' => 'safari',
      '/firefox/i' => 'firefox',
      '/msie/i' => 'internet-explorer',
      '/opera/i' => 'opera',
    ) as $key => $value) {
      if (preg_match($key, $agent)) {
        return $value;
      }
    }
    return null;
  }

}
