<?php

/**
 * Base class for localization. Can be overloaded ( method localize() ) to
 * implement translations from different sources, e.g. database, po/pot file,
 * ini file etc. The implementation has to take into account that constants
 * can be defined in the text using an associative array, e.g.
 *
 * localize("Here is a :contant inside", array(':constant' => 'REPLACED CONSTANT'))
 *
 * Constants have to start with one of ":", "!" or "@". All replacements that
 * are not string or numeric will be ignored
 *
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2005-2010
 * @license GPL
 * @version 1.0
 */

namespace sw;

class Localizer {

  /**
   * Language code definition, can be en-us, de-de etc.
   * @var string
   */
  protected $lang = '';

  /**
   * Stores the instance
   * @var Localizer
   */
  private static $instance = null;

  /**
   * Object's localization method
   * OVERLOAD THIS TO DEFINE YOUR LOCALIZER
   *
   * @param string $text
   * @param array $const
   */
  protected function localize($text, $const=array()) {
    if (empty($const)) {
      return $text;
    } else {
      return str_ireplace(array_keys($const), array_values($const), $text);
    }
  }

  /**
   * Constructor
   * @param string $language
   */
  public function __construct($language='') {
    $this->lang = trim($language);
  }

  /**
   * Returns and optionally sets the language code
   * @param string $lang=null
   * @return string
   */
  public static function language($lang=null) {
    if (!is_null($lang)) {
      self::$instance->lang = trim($lang);
    }
    $lang = strtolower(trim($lang));
    return self::$instance->lang;
  }

  /**
   * Initializes the localizer
   * @param mixed $localizer
   * @param string $language
   */
  public static final function start($localizer=null, $language='') {
    if (empty($localizer)) {
      self::$instance = new self();
    } else if (is_string($localizer)) {
      self::$instance = new $localizer(self::language($language));
    }
    if (!self::$instance instanceof self) {
      // Don't use translated exceptions here, there is no localizer yet
      throw new \Exception("Specified localization class is not derived from class Localizer");
    }
  }

  /**
   * Destroys the localizer instance
   */
  public static final function stop() {
    self::$instance = null;
  }

  /**
   * Returns the translated version of a text
   * @param string $text
   * @param array $const
   */
  public static final function translate($text, $const=array()) {
    if (empty($const)) {
      $const = array();
    } else {
      foreach ($const as $k => $v) {
        $const[$k] = (empty($k) || strpos(':!@', substr($k, 0, 1)) === false) ? null : print_r($v, true);
      }
      $const = array_filter($const, 'is_string');
    }
    if(!self::$instance) {
      return str_ireplace(array_keys($const), array_values($const), $text);
    } else {
      return self::$instance->localize($text, $const);
    }
  }
}
