<?php

/**
 * Localizable exception class.
 *
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2005-2010
 * @license GPL
 * @version 1.0
 */

namespace sw;

class LException extends \Exception {

  /**
   * Localized exception constructor
   * @param string $message
   * @param array $locale_const
   * @param int $code
   * @param Exception $previous
   */
  public function __construct($message='', $locale_const=array(), $code=null, $previous=null) {
    try {
      $message = Localizer::translate($message, $locale_const);
    } catch (\Exception $e) {
      if (is_array($locale_const)) {
        try {
          $message = str_ireplace(array_keys($locale_const), array_values($locale_const), $message);
        } catch (\Exception $e) {
          if (class_exists('sw\Tracer', false)) {
            Tracer::trace('!!!! Failed to replace constants in exception ' . $message);
          }
        }
      } else if (is_numeric($locale_const)) {
        $previous = $code;
        $code = $locale_const;
      }
    }
    parent::__construct($message, $code, $previous);
  }
}
