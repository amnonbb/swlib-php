<?php

/**
 * Wrapper for \Exception
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2006-2010
 * @license GPL
 * @version 1.0
 * @uses \Exception
 */

namespace sw;

class Exception extends \Exception {
}
