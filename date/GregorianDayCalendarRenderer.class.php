<?php

/**
 * Gegorian Day Calendar renderer class. Overwrite this class if you want a
 * different rendered representation.
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2010
 * @license GPL
 * @version 1.0
 * @uses GregorianCalendar
 * @uses GregorianCalendarEvent
 */

namespace sw;

class GregorianDayCalendarRenderer {

  /**
   * Defines the parent calendar
   * @var GregorianCalendar
   */
  private $parent = null;

  /**
   * The output of the rendering process
   * @var string
   */
  protected $output = '';

  /**
   * The HTML element id of the table
   * @var string
   */
  protected $tableId = '';

  /**
   * Constructor
   * @param GregorianCalendar $parentGregorianCalendar
   */
  public function __construct($parentGregorianCalendar = null) {
    $this->parent = $parentGregorianCalendar;
  }

  /**
   * Sets the parent of the calendar
   * @param GregorianCalendar $calendar
   */
  public final function setParent(GregorianCalendar $calendar) {
    if (!is_null($this->parent) && $this->parent !== $calendar) {
      throw new LException('You cannot re-assign the parent calendar instance');
    } else {
      $this->parent = $calendar;
    }
  }

  /**
   * Returns the parent Calendar object (which the renderer is for)
   * @return GregorianCalendar
   */
  public final function getParent() {
    return $this->parent;
  }

  /**
   * Sets the id of the table that contains the day calendar
   * @param string $id
   */
  public final function setTableId($id) {
    if (empty($id)) {
      $this->tableId = '';
    } else {
      $this->tableId = trim($id);
    }
  }

  /**
   * Returns the id of the table that contains the day calendar
   * @return string
   */
  public final function getTableId() {
    return $this->tableId;
  }

  /**
   * Returns the output of the rendering process
   * @return string
   */
  public final function getOutput() {
    return $this->output;
  }

  /**
   * Callback to start the calendar - every HTML content before the weeks
   * (and days in the weeks). Normally a table, thead, th with weekday names.
   * @param IDate $date
   * @param int $tick
   * @param int $displayFrom
   * @param int $dayStartsAt
   * @param int $dayEndsAt
   * @param int $displayTo
   * @param GregorianCalendarEvent[] &$events
   * @return void
   */
  public function onStart($date, $tick, $displayFrom, $dayStartsAt, $dayEndsAt, $displayTo, &$events) {
    $id = empty($this->tableId) ? '' : ('id="' . str_replace('#', '', $this->tableId) . '" ');
    $o = '';
    $o .= '<table ' . $id . 'class="gregorian-day-calendar">';
    $o .= '<caption>' . ucwords(strftime("%A, %B %e, %Y", $date->getTimeStamp())) . '</caption>';
    $this->output .= "$o\n";
  }

  /**
   * All HTML to close the table, table footer etc.
   * @return void
   */
  public function onEnd() {
    $this->output .= "</table>\n";
    $this->events = array();
  }

  /**
   * Renders one tick with the division period, padding means that the day
   * of the calendar has either not started yet or already ended (according
   * to $dayStartsAt and $dayEndsAt)
   * @param IDate $date
   * @param int $tick
   * @param bool $isPadding
   * @param bool $hidden
   * @param GregorianCalendarEvent[] &$events
   * @param array() &$eventMap
   * @return void
   */
  public function onTick(IDate $date, $tick, $isPadding, $hidden, &$events, &$eventMap) {
    if ($hidden)
      return;
    $class = array();
    if ($isPadding)
      $class[] = 'padding';
    if ($date->getMinute() == 0) {
      $class[] = 'full-hour';
      $fullHourTime = sprintf("%02d:%02d", $date->getHour(), $date->getMinute());
    } else {
      $fullHourTime = '&nbsp;';
    }

    $time = sprintf("%04d-%02d-%02d %02d:%02d", $date->getYear(), $date->getMonth(), $date->getDay(), $date->getHour(), $date->getMinute());

    if (!empty($events)) {
      $ts = $date->getTimeStamp();
      $row = &$eventMap[$ts];
      $td = '';
      $td .= '<td class="space"></td>' . "\t";
      foreach ($row as $index => $col) {
        if ($col === true) {
          $td .= '<td class="space"></td>' . "\t";
          $td .= '<!-- rowspan -->'; // event running, managed by rowspan
        } else if (is_null($col)) {
          $td .= '<!-- colspan -->'; // event running, managed by colspan
        } else if ($col === false) {
          $td .= '<td class="space"></td>' . "\t";
          $td .= '<td class="data">&nbsp;</td>'; // an empty cell
        } else if (is_numeric($col)) {
          $td .= '<td class="space"></td>' . "\t";
          $event = $events[$col]; // by ID
          // $rows = (ceil($event->getEnd() / $tick) - ($ts / $tick));
          $cols = count($row) - $index; // index starts at 0 --> min cols=1, max=count(...)
          $rows = 1;
          for ($i = $ts; $i < $ts + ($tick * count($eventMap)); $i+=$tick) {
            if (isset($eventMap[$i + $tick][$index]) && $eventMap[$i + $tick][$index] === true) {
              $rows++;
            } else {
              break;
            }
          }

          for ($i = $ts; $i < $ts + ($tick * $rows); $i+=$tick) {
            if (!isset($eventMap[$i][$index + 1]) || $eventMap[$i][$index + 1] !== false) {
              $cols = 1;
              break;
            }
          }

          if ($cols > 1) {
            for ($i = $ts; $i < $ts + ($tick * $rows); $i+=$tick) {
              for ($j = $index; $j < count($row); $j++) {
                $eventMap[$i][$j] = null;
              }
            }
            $row[$index] = $event->getId();
            // Take the spacers into account
            $cols = 2 * $cols - 1;
          }
          $td .= $this->renderEvent($event, $rows, $cols);
        }
        $td .= "\n";
      }
    } else {
      $td = '<td class="space"></td><td></td>';
    }

    $o = '';
    $o .= '<tr value="' . $time . '" ' . (empty($class) ? '' : (' class="' . implode(' ', $class)) . '"') . '>' . "\n";
    $o .= "\t" . '<td class="time" title="' . $time . '" >' . $fullHourTime . '</td>' . "\n";
    $o .= $td;
    $o .= '</tr>' . "\n";
    $this->output .= $o;
  }

  /**
   * Returns a day calendar string representation of an event
   * @param GregorianCalendarEvent $event
   * @param int $rows
   * @params int $cols
   * @return string
   */
  protected function renderEvent(GregorianCalendarEvent $event, $rows, $cols) {
    $td = '<td class="data event" rowspan="' . $rows . '" colspan="' . $cols . '">';
    $td .= (method_exists($event, 'render')) ? $event->render($this) : '';
    $td .= "</td>";
    return $td;
  }

}
