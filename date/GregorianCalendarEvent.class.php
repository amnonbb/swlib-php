<?php

/**
 * Base class for Gegorian Calendar Events. Events are defined using a unique
 * identifier (e.g. the primary key of a database or file), a start timestamp
 * and an end timestamp. The configuration of the GregorianCalendar class
 * decides if these timestamps are interpreted as local or UTC. Further "fixed"
 * properties are the type of event (e.g. "meeting", "festival" ...) and an
 * associative data array that contains variable information about the particular
 * event. All keys in the array can be accessed like properties (if e.g.
 *  $data = array(
 *      'where' => 'there',
 *      'who' => 'me',
 *      'why' => 'because'
 *  );
 *
 * Then $event->where === 'there'.
 * Note that you should choose "PHP-variable-name-conform" array keys for this,
 * alternatively you can use the data getter: $event->getData("who") === "me".
 *
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2010
 * @license GPL
 * @version 1.0
 */

namespace sw;

class GregorianCalendarEvent {

  /**
   * A unique identifier of the event
   * @var string
   */
  protected $id = '';

  /**
   * The name of the event
   * @var string
   */
  protected $name = 'New Event';

  /**
   * The timestamp when the event starts
   * @var IDate
   */
  protected $start = 0;

  /**
   * The timestamp when the event ends
   * @var IDate
   */
  protected $end = 0;

  /**
   * The type of event
   * @var string
   */
  protected $type = '';

  /**
   *
   * @var array
   */
  protected $data = array();

  /**
   * GregorianCalendarEvent constructor
   * @param string $name=''
   * @param mixed $start=null
   * @param mixed $end=null
   * @param array $data=array()
   * @param string $type=null
   * @param mixed $id=null
   */
  public function __construct($name='', $start=null, $end=null, array $data=array(), $type=null, $id=null) {
    if ($start instanceof IDate) {
      $start = $start->getTimeStamp();
    } else if (empty($start)) {
      $start = time();
    } else if (!is_numeric($start)) {
      throw new LException('GregorianCalendarEvent start time must be an integer timestamp');
    }

    if ($end instanceof IDate) {
      $end = $end->getTimeStamp();
    } else if (empty($end)) {
      $end = $start + 3600;
    } else if (!is_numeric($end)) {
      throw new LException('GregorianCalendarEvent end time must be an integer timestamp');
    }

    if ($end <= $start) {
      throw new LException('GregorianCalendarEvent end time must be later than the start time');
    }

    $this->start = $start;
    $this->end = $end;

    if (!empty($name))
      $this->name = trim($name);
    if (!empty($id))
      $this->id = $id;
    if (!empty($data))
      $this->data = $data;
    if (!empty($type))
      $this->type = trim($type); // implicit toString
  }

  /**
   * Returns a property if the correcponding getter (get<$ame>)exists OR the
   * key exsts in the data array. Returns null if none of both exists. Note
   * that values stored in the data array are inaccessible if a the key is
   * e.g. "from", "to", "name" etc.
   * @param string $name
   */
  public function __get($name) {
    if (method_exists($this, 'get' . $name)) {
      $name = 'get' . $name;
      return $this->$name();
    } else if (isset($this->data[$name])) {
      return $this->data[$name];
    } else {
      return null;
    }
  }

  /**
   * Sets a property if the correcponding setter (set<$ame>) exists. If no
   * setter is found, then the value will be stored in the data array. Note
   * that values stored in the data array are inaccessible if a the key is
   * e.g. "from", "to", "name" etc. The setter will be called instead.
   * @param string $name
   * @param mixed $value
   * @return void
   */
  public function __set($name, $value) {
    if (method_exists($this, 'set' . $name)) {
      $name = 'set' . $name;
      $this->$name($value);
    } else {
      $this->data[$name] = $value;
    }
  }

  /**
   * String representation
   * @return string
   */
  public function __toString() {
    return '['
            . strftime('%Y-%m-%d %H:%M:%S %Z', $this->start)
            . ' - '
            . strftime('%Y-%m-%d %H:%M:%S %Z', $this->end)
            . '] '
            . $this->name
            . ' {id="' . $this->id . '", type="' . $this->type
            . '", data=["' . implode('", "', $this->data) . '"]'
            . '}';
  }

  /**
   * Returns the id of the event
   * @return mixed
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Returns the name of the event
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Returns when the event starts
   * @return IDate
   */
  public function getStart() {
    return $this->start;
  }

  /**
   * Returns when the event ends
   * @return IDate
   */
  public function getEnd() {
    return $this->end;
  }

  /**
   * Returns the type of event
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Returns the data array or a valued in the data array specified using
   * $key, null if not found
   * @return mixed
   */
  public function getData($key=null) {
    if (!empty($key)) {
      return isset($this->data[$key]) ? $this->data[$key] : null;
    } else {
      return $this->data;
    }
  }

  /**
   * Sets the id of the event
   * @param mixed $id
   */
  public function setId($id) {
    if (!is_scalar($id)) {
      throw new LException('An calendar event ID cannot be an object or array');
    } else {
      $this->id = $id;
    }
  }

  /**
   * Sets the name of the event
   * @param string $name
   */
  public function setName($name) {
    $this->name = trim($name);
  }

  /**
   * Sets the timestamp when the event starts
   * @param int $from
   */
  public function setFrom($from) {
    if (is_numeric($from)) {
      $this->start = intval($from);
    } else if ($from instanceof IDate) {
      $this->start = $from->getTimeStamp();
    } else {
      throw new LException('"from"-data is no timestamp');
    }
  }

  /**
   * Sets when the event ends
   * @param int $to
   */
  public function setTo($to) {
    if (is_numeric($to)) {
      $this->end = intval($to);
    } else if ($to instanceof IDate) {
      $this->end = $to->getTimeStamp();
    } else {
      throw new LException('"to"-data is no timestamp');
    }
  }

  /**
   * Sets the event type
   * @param string $type
   */
  public function setType($type) {
    $this->type = trim(strtolower($type));
  }

  /**
   * Sets the custom/user data array
   * @param array $data
   */
  public function setData(array $data) {
    $this->data = $data;
  }

}
