<?php

/**
 * UTC/Local date interface. Implemented in class UtcDate, LocalDate.
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2005-2010
 * @license GPL
 * @version 1.0
 */

namespace sw;

interface IDate {

  /**
   * Returns year
   * @return int
   */
  public function getYear();

  /**
   * Returns month
   * @return int
   */
  public function getMonth();

  /**
   * Returns the day
   * @return int
   */
  public function getDay();

  /**
   * Returns hour
   * @return int
   */
  public function getHour();

  /**
   * Returns minute
   * @return int
   */
  public function getMinute();

  /**
   * Returns second
   * @return int
   */
  public function getSecond();

  /**
   * Returns unix time stamp
   * @return int
   */
  public function getTimeStamp();

  /**
   * Returns weekday
   * @return int
   */
  public function getWeekDay();

  /**
   * Sets year
   * @param int $year
   */
  public function setYear($year);

  /**
   * Sets month
   * @param int $month
   */
  public function setMonth($month);

  /**
   * Sets day
   * @param int $day
   */
  public function setDay($day);

  /**
   * Sets hour
   * @param int $hour
   */
  public function setHour($hour);

  /**
   * Sets minute
   * @param int $minute
   */
  public function setMinute($minute);

  /**
   * Sets second
   * @param int $second
   */
  public function setSecond($second);

  /**
   * Returns unix time stamp
   * @patram int $timestamp
   */
  public function setTimeStamp($timestamp);

  /**
   * Changes all properties != null and updates time stamp
   * @param int $year
   * @param int $month
   * @param int $day
   * @param int $hour
   * @param int $minute
   * @param int $second
   * @return void
   */
  public function setSerial($year=null, $month=null, $day=null, $hour=null, $minute=null, $second=null);

  /**
   * Returns this - another date, object is not affected
   * @param IDate $date
   * @return IDate
   */
  public function substract($date);

  /**
   * Returns this + another date, object is not affected
   * @param IDate $date
   * @return IDate
   */
  public function add($date);

  /**
   * Returns the date/time from stamp in days
   * @return double
   */
  public function inDays();

  /**
   * Returns the date/time from stamp in hours
   * @return double
   */
  public function inHours();

  /**
   * Returns the date/time from stamp in hours
   * @return double
   */
  public function inMinutes();

  /**
   * Returns the date/time from stamp in seconds
   * @return double
   */
  public function inSeconds();

  /**
   * Returns next 'month','day','hour','minute','second'.
   * Next month of 2008-01-24 13:12:14 returns 2008-02-01 00:00:00
   * @return IDate
   */
  public function getNext($unit='second');

  /**
   * Returns last 'month','day','hour','minute','second'.
   * @return IDate
   */
  public function getLast($unit='second');
}
