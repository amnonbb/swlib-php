<?php

/**
 * Gegorian Calendar event controller. Overwrite the methods to customize
 * the controller (load from file, from database etc. The class ist not
 * abstract because it will be instantiated by the calendar if no derived
 * controller is specified. This class methods returns empty date, which have
 * no effect.)
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2010
 * @license GPL
 * @version 1.0
 * @uses GregorianCalendarEvent
 */

namespace sw;

class GregorianCalendarEventController {

  /**
   * Returns all events that start and end between a given period. E.g. if
   * $from and $to are the timestamps for 00:00 to 23:59:59 on the same day,
   * then the method would return all events that take place exactly on this
   * day (start and end on this day).
   * @param mixed $from
   * @param mixed $to
   * @return GregorianCalendarEvent[]
   */
  public function getEventsBetween($from, $to) {
    return array();
  }

  /**
   * Returns all events that do not end before and do not start after the given
   * time period. If e.g. $from and $to are timespamps corresponding to 00:00
   * and 23::59:59 on the same day, then this method would return all events
   * that partially or completely take place on this day.
   * @param mixed $from
   * @param mixed $to
   * @return GregorianCalendarEvent[]
   */
  public function getEventsWhichMatch($from, $to) {
    return array();
  }

  /**
   * Returns an event given by its identifier
   * @return GregorianCalendarEvent
   */
  public function getEvent($id) {
    return null;
  }

}
