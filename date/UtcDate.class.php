<?php

/**
 * UTC refered dates and related operations.
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2005-2010
 * @license GPL
 * @version 1.0
 * @uses IDate
 */

namespace sw;

class UtcDate implements IDate {

  /**
   * Contains the unix timestamp
   * @var array
   */
  protected $mTimeStamp = 0;

  /**
   * Contains year
   * @var int
   */
  protected $mYear;

  /**
   * Contains month
   * @var int
   */
  protected $mMonth;

  /**
   * Contains day
   * @var int
   */
  protected $mDay;

  /**
   * Contains hour
   * @var int
   */
  protected $mHour;

  /**
   * Contains minute
   * @var int
   */
  protected $mMinute;

  /**
   * Contains second
   * @var int
   */
  protected $mSecond;

  /**
   * Contains day in week
   * @var int
   */
  protected $mWeekDay;

  /**
   * Class constructor
   * @param mixed $datetime
   */
  public function __construct($datetime=null) {
    if (empty($datetime)) {
      $this->mTimeStamp = strtotime(gmdate("M d Y H:i:s", time()));
    } else if ($datetime instanceof IDate) {
      $this->mTimeStamp = $datetime->getTimeStamp();
    } else if (is_numeric($datetime)) {
      $this->mTimeStamp = strtotime(gmdate("M d Y H:i:s", $datetime));
    } else if (!is_string($datetime)) {
      throw new LException('Date format is not valid');
    } else {
      $t = strtotime($datetime);
      if ($t === false) {
        throw new LException('Cannot parse the passed date text');
      } else {
        $this->mTimeStamp = strtotime(gmdate("M d Y H:i:s", $t));
      }
    }
    $this->update();
  }

  /**
   * Updates properties by timestamp
   * @return void
   */
  protected function update() {
    $dt = getdate($this->mTimeStamp);
    $this->mSecond = $dt['seconds'];
    $this->mMinute = $dt['minutes'];
    $this->mHour = $dt['hours'];
    $this->mDay = $dt['mday'];
    $this->mMonth = $dt['mon'];
    $this->mYear = $dt['year'];
    $this->mWeekDay = $dt['wday'] < 1 ? 7 : $dt['wday'];
  }

  /**
   * Updates timestamp by properties
   * @return void
   */
  protected function updateTimeStamp() {
    $this->mTimeStamp = mktime($this->mHour, $this->mMinute, $this->mSecond, $this->mMonth, $this->mDay, $this->mYear, 0);
  }

  /**
   * Returns format "YYYY-MM-DD HH:MM:SS GMT"
   * @return string
   */
  public function toString() {
    return sprintf("%4d-%02d-%02d %02d:%02d:%02d %3s", $this->getYear(), $this->getMonth(), $this->getDay(), $this->getHour(), $this->getMinute(), $this->getSecond(), $this->getTimeZoneName()
    );
  }

  /**
   * Returns format  "YYYY-MM-DD"
   * @return string
   */
  public function toDateString() {
    return sprintf("%4d-%02d-%02d", $this->getYear(), $this->getMonth(), $this->getDay()
    );
  }

  /**
   * Returns format "HH:MM::SS"
   * @return string
   */
  public function toTimeString() {
    return sprintf("%02d:%02d:%02d", $this->getHour(), $this->getMinute(), $this->getSecond()
    );
  }

  /**
   * Returns format "YYYY-MM-DD HH:MM GMT"
   * @return string
   */
  public function __toString() {
    return $this->toString();
  }

  /**
   * Returns year
   * @return int
   */
  public function getYear() {
    return $this->mYear;
  }

  /**
   * Returns month
   * @return int
   */
  public function getMonth() {
    return $this->mMonth;
  }

  /**
   * Returns day
   * @return int
   */
  public function getDay() {
    return $this->mDay;
  }

  /**
   * Returns hour
   * @return int
   */
  public function getHour() {
    return $this->mHour;
  }

  /**
   * Returns minute
   * @return int
   */
  public function getMinute() {
    return $this->mMinute;
  }

  /**
   * Returns second
   * @return int
   */
  public function getSecond() {
    return $this->mSecond;
  }

  /**
   * Returns unix time stamp
   * @return int
   */
  public function getTimeStamp() {
    return $this->mTimeStamp;
  }

  /**
   * Returns the time zone abbreciation
   * @return string
   */
  public function getTimeZoneName() {
    return "GMT";
  }

  /**
   * Returns weekday
   * @return int
   */
  public function getWeekDay() {
    return $this->mWeekDay;
  }

  /**
   * Sets year
   * @param int $year
   */
  public function setYear($year) {
    $this->mYear = $year;
    $this->updateTimeStamp();
  }

  /**
   * Sets month
   * @param int $month
   */
  public function setMonth($month) {
    $this->mMonth = $month;
    $this->updateTimeStamp();
  }

  /**
   * Sets day
   * @param int $day
   */
  public function setDay($day) {
    $this->mDay = $day;
    $this->updateTimeStamp();
  }

  /**
   * Sets hour
   * @param int $hour
   */
  public function setHour($hour) {
    $this->mHour = $hour;
    $this->updateTimeStamp();
  }

  /**
   * Sets minute
   * @param int $minute
   */
  public function setMinute($minute) {
    $this->mMinute = $minute;
    $this->updateTimeStamp();
  }

  /**
   * Sets second
   * @param int $second
   */
  public function setSecond($second) {
    $this->mSecond = $second;
    $this->updateTimeStamp();
  }

  /**
   * Sets the timestamp
   * @param int $timestamp
   */
  public function setTimeStamp($timestamp) {
    $this->mTimeStamp = $timestamp;
    $this->update();
  }

  /**
   * Changes all properties != null and updates time stamp
   * @param int $year
   * @param int $month
   * @param int $day
   * @param int $hour
   * @param int $minute
   * @param int $second
   * @return void
   */
  public function setSerial($year=null, $month=null, $day=null, $hour=null, $minute=null, $second=null) {
    if ($year !== null)
      $this->mYear = $year;
    if ($month !== null)
      $this->mMonth = $month;
    if ($day !== null)
      $this->mDay = $day;
    if ($hour !== null)
      $this->mHour = $hour;
    if ($minute !== null)
      $this->mMinute = $minute;
    if ($second !== null)
      $this->mSecond = $second;
    $this->updateTimeStamp();
  }

  /**
   * Returns this - another date, object is not affected, $date can also be
   * a timestamp or period in seconds
   * @param IDate $date
   * @return UtcDate
   */
  public function substract($date) {
    $class = get_class($this);
    if (is_numeric($date)) {
      return new $class($this->getTimeStamp() - intval($date));
    } else if ($date instanceof IDate) {
      return new $class($this->getTimeStamp() - $date->getTimeStamp());
    } else {
      throw new LException('Only timestamps or IDate can be substracted from an IDate');
    }
  }

  /**
   * Returns this + another date, object is not affected, $date can also be
   * a timestamp or a period in seconds
   * @param IDate $date
   * @return UtcDate
   */
  public function add($date) {
    $class = get_class($this);
    if (is_numeric($date)) {
      return new $class($this->getTimeStamp() + intval($date));
    } else if ($date instanceof IDate) {
      return new $class($this->getTimeStamp() + $date->getTimeStamp());
    } else {
      throw new LException('Only timestamps or IDate can be added to an IDate');
    }
  }

  /**
   * Returns the date/time from stamp in days
   * @return double
   */
  public function inWeeks() {
    return round($this->getTimeStamp() / 604800.0, 2);
  }

  /**
   * Returns the date/time from stamp in days
   * @return double
   */
  public function inDays() {
    return round($this->getTimeStamp() / 86400.0, 2);
  }

  /**
   * Returns the date/time from stamp in hours
   * @return double
   */
  public function inHours() {
    return round($this->getTimeStamp() / 3600.0, 2);
  }

  /**
   * Returns the date/time from stamp in hours
   * @return double
   */
  public function inMinutes() {
    return round($this->getTimeStamp() / 60.0, 2);
  }

  /**
   * Returns the date/time from stamp in seconds
   * @return double
   */
  public function inSeconds() {
    return $this->getTimeStamp();
  }

  /**
   * Returns next 'month','day','hour','minute','second'.
   * Next month of 2008-01-24 13:12:14 returns 2008-02-01 00:00:00
   * @return UtcDate
   */
  public function getNext($unit='second') {
    $unit = strtolower(trim($unit, " \n\r\t"));
    $year = null;
    $month = null;
    $day = null;
    $hour = null;
    $minute = null;
    $second = null;
    // Do not insert breaks here ...
    switch ($unit) {
      case 'month':
        $day = 1;
      case 'week':
      case 'day':
      case 'sunday':
      case 'monday':
      case 'tuesday':
      case 'wednesday':
      case 'thursday':
      case 'friday':
      case 'saturday':
        $hour = 0;
      case 'hour':
        $minute = 0;
      case 'minute':
        $second = 0;
      case 'second':
        break;
      default:
        throw new LException("argument unit is not valid: ':unit'", array(':unit' => $unit));
    }
    $class = get_class($this);
    $d = new $class(strtotime("next $unit", $this->getTimeStamp()));
    $d->setSerial($year, $month, $day, $hour, $minute, $second);
    return $d;
  }

  /**
   * Returns last 'month','day','hour','minute','second'.
   * Next last of 2008-01-24 13:12:14 returns 2007-12-01 00:00:00
   * @return UtcDate
   */
  public function getLast($unit='second') {
    $unit = strtolower(trim($unit, " \n\r\t"));
    $year = null;
    $month = null;
    $day = null;
    $hour = null;
    $minute = null;
    $second = null;
    // Do not insert breaks here ...
    switch ($unit) {
      case 'month':
        $day = 1;
      case 'week':
      case 'day':
      case 'sunday':
      case 'monday':
      case 'tuesday':
      case 'wednesday':
      case 'thursday':
      case 'friday':
      case 'saturday':
        $hour = 0;
      case 'hour':
        $minute = 0;
      case 'minute':
        $second = 0;
      case 'second':
        break;
      default:
        throw new LException("argument unit is not valid: ':unit'", array(':unit' => $unit));
    }
    $class = get_class($this);
    $d = new $class(strtotime("last $unit", $this->getTimeStamp()));
    $d->setSerial($year, $month, $day, $hour, $minute, $second);
    return $d;
  }

}
