<?php

/**
 * Gegorian Month Calendar renderer. Overwrite this class if you want to have an
 * alterantive rendered representation of the month calendar.
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2010
 * @license GPL
 * @version 1.0
 * @uses GregorianCalendar
 * @uses GregorianCalendarEvent
 */

namespace sw;

class GregorianMonthCalendarRenderer {

  /**
   * Defines the parent calendar
   * @var GregorianCalendar
   */
  private $parent = null;

  /**
   * The output of the rendering process
   * @var string
   */
  protected $output = '';

  /**
   * Constructor
   * @param GregorianCalendar $parentGregorianCalendar
   */
  public function __construct($parentGregorianCalendar = null) {
    $this->parent = $parentGregorianCalendar;
  }

  /**
   * Sets the parent of the calendar
   * @param GregorianCalendar $calendar
   */
  public final function setParent(GregorianCalendar $calendar) {
    if (!is_null($this->parent) && $this->parent !== $calendar) {
      throw new LException('You cannot re-assign the parent calendar instance');
    } else {
      $this->parent = $calendar;
    }
  }

  public final function getParent() {
    return $this->parent;
  }

  /**
   * Returns the output of the rendering process
   * @return string
   */
  public final function getOutput() {
    return $this->output;
  }

  /**
   * Callback to start the calendar - every HTML content before the weeks
   * (and days in the weeks). Normally a table, thead, th with weekday names.
   * @return void
   */
  public function onStart() {
    $date = $this->getParent()->getDateToShow();
    $o = '';
    $o .= '<table class="gregorian-month-calendar">';
    $o .= '<caption>';
    $o .= '<a class="prev" href="' . $_SERVER['PHP_SELF'] . '?month-calendar-show=' . $date->getLast('month')->toDateString() . '">' . '&nbsp;' . '</a>';
    $o .= '<a class="next" href="' . $_SERVER['PHP_SELF'] . '?month-calendar-show=' . $date->getNext('month')->toDateString() . '">' . '&nbsp;' . '</a>';
    $o .= '<a class="title">' . ucfirst(strftime("%B %Y", $date->getTimeStamp())) . '</a>';
    $o .= '</caption>';
    for ($i = 1; $i <= 7; $i++) {
      $o .= '<col class="' . $this->getParent()->getWeekDayAbbreviation($i) . '" />';
    }
    $o .= '<tr>';
    foreach ($this->getParent()->getWeekDayIndex() as $i) {
      $o .= '<th><a>' . strtoupper(substr(gmstrftime('%A', ($i - 4) * 24 * 3600), 0, 1)) . '</a></th>';
    }
    $o .= '</tr>';
    $this->output .= $o;
  }

  /**
   * All HTML to close the table, table footer etc.
   * @return void
   */
  public function onEnd() {
    $this->output .= '</table>';
  }

  /**
   * Start a new week, normally a <tr> or an additional <td></td> with the
   * calendar week (e.g. CW45) etc.
   * @return void
   */
  public function onWeekStart($weekId) {
    $this->output .= '<tr>';
  }

  /**
   * Finish a new week, normally a </tr>
   * @return void
   */
  public function onWeekEnd($weekId) {
    $this->output .= "</tr>\n";
  }

  /**
   * Renders a day in the week (normally a <tr>$dateDayOfMonthTwoDigits</tr>
   * @param IDate $day
   * @param bool isPadding
   * @param bool isSelected
   * @param GregorianMonthCalendarRenderer[] $events
   * @return mixed
   */
  public function onDay(IDate $day, $isPadding, $isSelected, $events) {
    $class = array();
    $class[] = $this->getParent()->getWeekDayAbbreviation($day->getWeekDay());
    switch (Math::signz($day->getTimeStamp() - $this->getParent()->getToday()->getTimeStamp())) {
      case -1: $class[] = 'passed';
        break;
      case 0 : $class[] = 'today';
        break;
    }
    if (!empty($events))
      $class[] = 'has-events';
    if ($isPadding)
      $class[] = 'padding';
    if ($isSelected)
      $class[] = 'selected';
    $class = 'class="' . implode(' ', $class) . '"';
    $title = 'title="' . ucwords(strftime("%A, %B %e, %Y", $day->getTimeStamp())) . (count($events) == 0 ? '' : ' (' . count($events) . ' events)') . '"';
    $date = '<a href="' . $_SERVER['PHP_SELF'] . '?month-calendar-select=' . $day->toDateString() . '">' . sprintf("%02d", $day->getDay()) . '</a>';
    $day->isPadding = $isPadding;
    $this->output .= "<td $class $title>$date</td>";
  }

}
