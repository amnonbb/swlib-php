<?php

/**
 * Local time refered dates: same as UtcDate for local date/times. It is
 * assumed that $datetime (if numeric) is a local timestamp (form time() )
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2005-2010
 * @license GPL
 * @version 1.0
 * @uses UtcDate
 * @uses IDate
 */

namespace sw;

class LocalDate extends UtcDate implements IDate {

  /**
   * Class constructor
   * @param mixed $datetime
   */
  public function __construct($datetime=null) {
    if (empty($datetime)) {
      $this->mTimeStamp = time();
    } else if (is_numeric($datetime)) {
      $this->mTimeStamp = intval($datetime);
    } else if ($datetime instanceof IDate) {
      $this->mTimeStamp = $datetime->getTimeStamp();
    } else if (!is_string($datetime)) {
      throw new LException('Date format is not valid');
    } else {
      $t = strtotime($datetime);
      if ($t === false) {
        throw new LException('Cannot parse the passed date text');
      } else {
        $this->mTimeStamp = $t;
      }
    }
    $this->update();
  }

  /**
   * Returns the time zone abbreciation
   * @return string
   */
  public function getTimeZoneName() {
    return trim(strtoupper(date("T", $this->mTimeStamp)));
  }

  /**
   * Updates timestamp by properties
   * @return void
   */
  protected function updateTimeStamp() {
    $this->mTimeStamp = mktime($this->mHour, $this->mMinute, $this->mSecond, $this->mMonth, $this->mDay, $this->mYear, -1);
  }

}
