<?php

/**
 * Gegorian Calendar management with the ability to render day calendars and
 * month calendars using the corresponding rendering classes. Also manages
 * the events for selected time spans.
 * @gpackage de.atwillys.sw.php.swlib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2010
 * @license GPL
 * @version 1.0
 * @uses GregorianCalendarEvent
 * @uses GregorianCalendarEventController
 * @uses GregorianDayCalendarRenderer
 * @uses GregorianMonthCalendarRenderer
 */

namespace sw;

class GregorianCalendar {

  /**
   * The date (and time) to show.
   * @var IDate
   */
  private $dateToShow = null;

  /**
   * The (real) today, set this only in the constructor for history purposes
   * @var IDate
   */
  private $today = null;

  /**
   * The (real) date which is selected, e.g. to displad a day calendar
   * @var IDate
   */
  private $dateSelected = null;

  /**
   * The class to instantiate, must be an IDate implementation, normally
   * LocalDate or UtcDate
   * @var IDate
   */
  private $dateClass = 'LocalDate';

  /**
   * Defines if the week starts with Sunday or Monday
   * @var bool
   */
  private $weekStartsWithSunday = false;

  /**
   * A numeric array which contains the weekday indices for using with localized
   * day name PHP functions.
   * @var array
   */
  private $weekDayIndex = array(1, 2, 3, 4, 5, 6, 0);

  /**
   * Contains the weekday abbreviations
   * @var array
   */
  private $weekDayAbbr = array('sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat');

  /**
   * Defines the time period of division in a day calendar (in seconds).
   * @var int
   */
  private $hourDivision = 3600;

  /**
   * Defines when the day starts for the calendar (timestamp format in seconds)
   * @var int
   */
  private $dayStartsAt = '00:00:00';

  /**
   * Defines when the day ends for the calendar (timestamp format in seconds)
   * @var int
   */
  private $dayEndsAt = '23:59:59';

  /**
   * Every time between 00:00:00 and $dayDisplayedFrom (les than $dayDisplayedFrom)
   * will not be displayed
   * @var int
   */
  private $dayDisplayedFrom = '00:00:00';

  /**
   * Every time > $dayDisplayedTo to '23:59:59' will not be displayed
   * @var int
   */
  private $dayDisplayedTo = '23:59:59';

  /**
   * Reference to the event controller object
   * @var GregorianCalendarEventController
   */
  private $eventController = null;

  /**
   * Constructor
   * @param IDate $dateToShow
   * @param IDate $today
   * @param string $dateClass
   */
  public function __construct($dateToShow=null, $today=null, $dateClass='LocalDate') {
    $this->dateClass = $dateClass;
    $this->setToday($today);
    $this->setDateToShow($dateToShow);
    $this->setWeekStartsOnSunday(false);
    $this->setDayStartsAt($this->dayStartsAt);
    $this->setDayEndsAt($this->dayEndsAt);
    $this->setDayDisplayFrom($this->dayDisplayedFrom);
    $this->setDayDisplayTo($this->dayDisplayedTo);
    $this->setSelectedDate($this->getToday());
  }

  /**
   * Returns of a time $t is/takes place at the day $day. The day must be normed
   * to 00:00:00h.
   * @param IDate $day
   * @param IDate $t
   * @return bool
   */
  protected final function isSameDay(IDate $day, IDate $t) {
    return ($t->getTimeStamp() >= $day->getTimeStamp()) && ($t < $day->getTimeStamp() + 86400);
  }

  /**
   * Returns the date specified as today (which is normally today, but can
   * be specified for historic date referencing)
   * @return IDate
   */
  public final function getToday() {
    return $this->today;
  }

  /**
   * Returns the date specified as selected (e.g. for displaying a day calendar)
   * @return IDate
   */
  public final function getSelectedDate() {
    return $this->dateSelected;
  }

  /**
   * Returns the date which has to be displayed. This can be a time with hours
   * minues and seconds. The renderers have to adjust the displayed interval.
   * For a month calendar, the calenar will e.g. render the month July 2010 if
   * $dateToShow is 2010-07-15 03:05:30.
   */
  public final function getDateToShow() {
    return $this->dateToShow;
  }

  /**
   * Returns if the week shall start on Sunday (or Monday)
   * @return bool
   */
  public final function getWeekStartsOnSunday() {
    return $this->weekStartsWithSunday;
  }

  /**
   * Returns the week day index dependent on $weekStartsWithSunday, either
   * [0,1,2,3,4,5,6] (if Sun first day) or [1,2,3,4,5,6,0] (if Mon first day)
   * @return bool
   */
  public final function getWeekDayIndex() {
    return $this->weekDayIndex;
  }

  /**
   * Returns the week day abbreviation depending on $weekStartsWithSunday
   * @param int $index
   * @return string
   */
  public final function getWeekDayAbbreviation($index) {
    $index -= 1;
    if (!isset($this->weekDayIndex[$index])) {
      throw new LException("No such week day index: $index");
    } else {
      return $this->weekDayAbbr[$this->weekDayIndex[$index]];
    }
  }

  /**
   * Retuens the time period of on division in a day calendar (in seconds).
   * @return int
   */
  public function getHourDivision() {
    return $this->hourDivision;
  }

  /**
   * Returns when the day starts for the calendar
   * @return int
   */
  public final function getDayStartsAt() {
    return $this->dayStartsAt;
  }

  /**
   * Returns when the day ends for the calendar
   * @return int
   */
  public final function getDayEndsAt() {
    return $this->dayEndsAt;
  }

  /**
   * Every time between 00:00:00 and getDayDisplayedFrom()
   * will not be displayed
   * @return int
   */
  public final function getDayDisplayedFrom() {
    return $this->dayDisplayedFrom;
  }

  /**
   * Every time > getDayDisplayedTo() to '23:59:59' will not be displayed
   * @return int
   */
  public final function getDayDisplayedTo() {
    return $this->dayDisplayedTo;
  }

  /**
   * Returns a reference to the event controller, creates a standard controller
   * of no controller instantiated/set.
   * @return GregorianCalendarEventController
   */
  public final function getEventController() {
    if (!$this->eventController instanceof GregorianCalendarEventController) {
      $this->eventController = new GregorianCalendarEventController();
    }
    return $this->eventController;
  }

  /**
   * Returns the date specified as today (which is normally today, but can
   * be specified for historic date referencing)
   * @param mixed $today
   */
  public final function setToday($today) {
    $today = is_null($today) ? (new $this->dateClass()) : (new $this->dateClass(strval($today)));
    if (!($today instanceof IDate)) {
      throw new LException('Date classes used here must implement IDate');
    }
    $this->today = $today;
    $this->today->setSerial(null, null, null, 0, 0, 0);
  }

  /**
   * Sets the date specified as selected (e.g. for displaying a day calendar)
   * @return IDate
   */
  public final function setSelectedDate($selected) {
    $selected = is_null($selected) ? (new $this->dateClass()) : (new $this->dateClass(strval($selected)));
    if (!($selected instanceof IDate)) {
      throw new LException('Date classes used here must implement IDate');
    }
    $this->dateSelected = $selected;
  }

  /**
   * Stets the date which has to be displayed. This can be a time with hours
   * minues and seconds. The renderers have to adjust the displayed interval.
   * For a month calendar, the calenar will e.g. render the month July 2010 if
   * $dateToShow is 2010-07-15 03:05:30.
   * @param mixed $dateToShow
   */
  public final function setDateToShow($dateToShow) {
    $dateToShow = !is_null($dateToShow) ? (new $this->dateClass(strval($dateToShow))) : (($this->today instanceof IDate) ? clone $this->today : new $this->dateClass());
    if (!($dateToShow instanceof IDate)) {
      throw new LException('Date classes used here must implement IDate');
    }
    $this->dateToShow = $dateToShow;
  }

  /**
   * Sets if the week shall start on Sunday (or Monday)
   * @param bool $bool
   */
  public final function setWeekStartsOnSunday($bool) {
    $this->weekStartsWithSunday = $bool ? true : false;
    $this->weekDayIndex = $this->weekStartsWithSunday ? array(0, 1, 2, 3, 4, 5, 6) : array(1, 2, 3, 4, 5, 6, 0);
  }

  /**
   * Sets when the day starts for the calendar
   * @param mixed $time
   */
  public final function setDayStartsAt($time) {
    $time = new $this->dateClass($time);
    $this->dayStartsAt = 3600 * $time->getHour() + 60 * $time->getMinute() + $time->getSecond();
  }

  /**
   * Sets when the day ends for the calendar
   * @param mixed $time
   */
  public final function setDayEndsAt($time) {
    $time = new $this->dateClass($time);
    $this->dayEndsAt = 3600 * $time->getHour() + 60 * $time->getMinute() + $time->getSecond();
  }

  /**
   * Every time between 00:00:00 and $time (les than $time)
   * will not be displayed
   * @param mixed $time
   */
  public final function setDayDisplayFrom($time) {
    $time = new $this->dateClass($time);
    $this->dayDisplayedFrom = 3600 * $time->getHour() + 60 * $time->getMinute() + $time->getSecond();
  }

  /**
   * Every time > $time to '23:59:59' will not be displayed
   * @param mixed $time
   */
  public final function setDayDisplayTo($time) {
    $time = new $this->dateClass($time);
    $this->dayDisplayedTo = 3600 * $time->getHour() + 60 * $time->getMinute() + $time->getSecond();
  }

  /**
   * Sets a new event controller
   * @param GregorianCalendarEventController $controller
   */
  public final function setEventController($controller) {
    if (is_null($controller) || $controller instanceof GregorianCalendarEventController) {
      $this->eventController = $controller;
    } else {
      throw new LException('Calendar event controllers must be null (to unset) or GregorianCalendarEventController');
    }
  }

  /**
   * Sets the time period of on division in a day calendar (in seconds).
   * @param int $period
   */
  public function setHourDivision($period) {
    if (!is_numeric($period)) {
      throw new LException('Day division period must be numeric');
    } else {
      $period = intval($period);
      if ($period <= 0) {
        throw new LException('Day division period must be > 0');
      } else if (3600 % $period != 0) {
        throw new LException('An special amount of division periods must fit exactly 1 hour (3600 MOD period = 0)');
      } else {
        $this->hourDivision = $period;
      }
    }
  }

  /**
   * Returns a rendered representation of a month calendar
   * @param GregorianMonthCalendarRenderer $renderer
   * @return array
   */
  public final function renderMonthCalendar(GregorianMonthCalendarRenderer $renderer) {
    if (!$renderer instanceof GregorianMonthCalendarRenderer) {
      throw new LException('You must use a GregorianMonthCalendarRenderer to render this');
    }
    $renderer->setParent($this);
    $monthStart = new LocalDate($this->dateToShow->getYear() . "-" . $this->dateToShow->getMonth() . "-01");
    $monthEnd = $monthStart->getNext('month');
    $monthEnd->setTimeStamp($monthEnd->getTimeStamp() - 1);
    $calStart = $monthStart->getLast($this->weekStartsWithSunday ? 'sunday' : 'monday');
    $calEnd = $monthEnd->getNext($this->weekStartsWithSunday ? 'sunday' : 'monday');

    if ($calEnd->inWeeks() - $calStart->inWeeks() < 6) {
      $calEnd = $calEnd->getNext($this->weekStartsWithSunday ? 'sunday' : 'monday');
    }
    $calEnd->setTimeStamp($calEnd->getTimeStamp() - 1);

    $selected = clone $this->getSelectedDate();
    $selected->setSerial(null, null, null, 0, 0, 0);
    $selected = $selected->getTimeStamp();

    Tracer::trace('TZ           = ' . $monthEnd->getTimeZoneName());
    Tracer::trace('Date to show = ' . $this->dateToShow . '/' . $this->dateToShow->getWeekDay());
    Tracer::trace('Month start  = ' . $monthStart . '/' . $monthStart->getWeekDay());
    Tracer::trace('Month end    = ' . $monthEnd . '/' . $monthEnd->getWeekDay());
    Tracer::trace('Cal start    = ' . $calStart . '/' . $calStart->getWeekDay());
    Tracer::trace('Cal end      = ' . $calEnd . '/' . $calEnd->getWeekDay());
    Tracer::trace('Cal selected = ' . $this->getSelectedDate() . '/' . $selected);
    Tracer::trace("Num of weeks = " . ($calEnd->inWeeks() - $calStart->inWeeks()));

    $today = clone $this->today;
    $ts_s = $monthStart->getTimeStamp();
    $ts_e = $monthEnd->getTimeStamp();
    $wday = $week = 0;

    $events = array();
    $dayPeriod = 24 * 3600 - 1;

    $renderer->onStart();
    $renderer->onWeekStart(0);
    for ($day = new LocalDate($calStart); $day->getTimeStamp() <= $calEnd->getTimeStamp(); $day = $day->getNext('day')) {
      if ($wday > 6) {
        $wday = 0;
        $renderer->onWeekEnd($week++);
        $renderer->onWeekStart($week);
      }
      $events = $this->getEventController()->getEventsWhichMatch($day, $day->getTimeStamp() + $dayPeriod);
      $ts = $day->getTimeStamp();
      $renderer->onDay(clone $day, $ts < $ts_s || $ts > $ts_e, $ts == $selected, $events);
      $wday++;
    }
    $renderer->onEnd();
    return $renderer->getOutput();
  }

  /**
   * Returns a rendered representation of a day calendar
   * @param GregorianDayCalendarRenderer $renderer
   * @param $date=null
   */
  public final function renderDayCalendar(GregorianDayCalendarRenderer $renderer, $date=null) {
    if (!$renderer instanceof GregorianDayCalendarRenderer) {
      throw new LException('You must use a GregorianDayCalendarRenderer to render this');
    }
    if (!is_null($date)) {
      $date = new $this->dateClass($date);
    } else {
      $date = clone $this->getSelectedDate();
    }

    $renderer->setParent($this);
    $dayStart = new $this->dateClass($date);
    $dayEnd = new $this->dateClass($date);
    $dayStart->setSerial(null, null, null, 0, 0, 0);
    $dayEnd->setSerial(null, null, null, 23, 59, 59);
    $dayStartsAt = $dayStart->getTimeStamp() + $this->getDayStartsAt();
    $dayEndsAt = $dayStart->getTimeStamp() + $this->getDayEndsAt();
    $displayFrom = $dayStart->getTimeStamp() + $this->getDayDisplayedFrom();
    $displayTo = $dayStart->getTimeStamp() + $this->getDayDisplayedTo();
    $tick = $this->getHourDivision();

    Tracer::trace('Date to show = ' . $date);
    Tracer::trace('Day start    = ' . $dayStart . '/' . $dayStart->toTimeString());
    Tracer::trace('Day end      = ' . $dayEnd . '/' . $dayEnd->toTimeString());
    Tracer::trace('Cal start    = ' . $dayStartsAt . '/' . ($this->getDayStartsAt() / 3600));
    Tracer::trace('Cal end      = ' . $dayEndsAt . '/' . ($this->getDayEndsAt() / 3600));

    $events = $this->getEventController()->getEventsWhichMatch($displayFrom, $displayTo);
    $map = array();

    if (!empty($events)) {

      // Conditionize events: Sort the events by timestamp, assure that keys
      // are the the start timestamps.
      function lmb_sortCallback($a, $b) {
        return $a->getStart() > $b->getStart() ? 1 : -1;
      }

      usort($events, 'lmb_sortCallback');
      $ev = $events;
      $events = array();
      foreach ($ev as $event) {
        $events[$event->getId()] = $event;
      }
      unset($ev);

      foreach ($events as $event)
        Tracer::trace("EVENT: $event");

      // Generate a 2D map, where the row keys are the tick timestamps
      $map = array();
      for ($i = $displayFrom; $i <= $displayTo; $i+=$tick) {
        $map[$i] = array();
      }

      // Place the events in the map, beginning in column 0, first all events
      // that do not overlap (to shrink the loop after that)
      $ev = $events;
      while (!empty($ev)) {
        $event = array_shift($ev);
        $row = $tick * floor($event->getStart() / $tick);

        if ($row < $displayFrom) {
          $row = $displayFrom;
          $events[$row] = $event;
        }

        $end = $tick * ceil($event->getEnd() / $tick);
        $id = $event->getId();
        $col = 0;
        // Loop over all known columns to check if there is space ...
        while (isset($map[$row][$col])) {
          // Check if ther is space ...
          $fits = true;
          for ($i = $row; $i <= $end; $i+=$tick) {
            if ($map[$i][$col]) {
              // Not here, try next column
              $fits = false;
              break;
            }
          }
          if ($fits) {
            break;
          } else {
            $col++;
          }
        }
        // New column required, create it for the whole day
        if (!isset($fits) || !$fits) {
          for ($i = $displayFrom; $i <= $displayTo; $i+=$tick) {
            $map[$i][$col] = false;
          }
        }
        // Place the event in the row and column (and the rows that the
        // event takes place, mark the start row as ID, all other as
        // true (means "busy")
        $map[$row][$col] = $id;
        for ($i = $row + $tick; $i < $end; $i+=$tick) {
          $map[$i][$col] = true;
        }
      }

      // Cleanup
      unset($ev, $fits, $col, $row, $id, $end, $event, $i, $key, $nCols, $nEvents);

//            // Trace the map
//            foreach($map as $key => $row) {
//                $o = strftime("%H:%M:%S", $key) . " = ";
//                foreach($row as $id) {
//                    if($id === true) {
//                        $o .= ' busy';
//                    } else if($id === false) {
//                        $o .= ' free';
//                    } else {
//                        $o .= " " . sprintf("%04d", $id);
//                    }
//                }
//                Tracer::trace_r($o);
//            }
//            unset($o, $key, $row, $id);
    }

    $renderer->onStart($date, $tick, $displayFrom, $dayStartsAt, $dayEndsAt, $displayTo, $events);
    for ($time = $dayStart->getTimeStamp(); $time <= $dayEnd->getTimeStamp(); $time += $tick) {
      $renderer->onTick(new $this->dateClass($time), $tick, $time < $dayStartsAt || $time > $dayEndsAt, $time < $displayFrom || $time > $displayTo, $events, $map);
    }
    $renderer->onEnd();
    return $renderer->getOutput();
  }

}

?>