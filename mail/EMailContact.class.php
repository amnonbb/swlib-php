<?php

/**
 * EMail contact is a base class for the class Mailer. It can be easily be
 * extended to any other addressbook or user class (Note that it might be
 * be better to write an own user class and implement IEMailContact instead
 * of using this class).
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2006-2010
 * @license GPL
 * @version 1.0
 * @uses IEMailContact
 */

namespace sw;

class EMailContact implements IEMailContact {

  /**
   * Name
   * @var string
   */
  private $name;

  /**
   * Email address
   * @var string
   */
  private $address;

  /**
   * Name getter
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Name setter
   * @param string $name
   */
  public function setName($name) {
    if (!isset($name) || $name == "") {
      throw new LException("Name of email contact is invalid");
    }
    $this->name = $name;
  }

  /**
   * EMail address getter
   * @return string
   */
  public function getEMailAddress() {
    return $this->address;
  }

  /**
   * EMail address setter
   * @param string $address
   */
  public function setEMailAddress($address) {
    if (!isset($address) || $address == "") {
      throw new LException("EMail addresss is invalid");
    } else if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $address)) {
      throw new LException("EMail addresss is invalid");
    }
    $this->name = $address;
  }

  /**
   * Nonmagic string representator
   * @return string
   */
  public function toEMailAddressString() {
    return $this->name . " <" . $this->address . ">";
  }

  /**
   * EMailContact constructor
   * @param string $name
   * @param string $address
   */
  public function __construct($name="", $address="") {
    $this->name = $name;
    $this->address = $address;
  }

  public function __toString() {
    return $this->toEMailAddressString();
  }

}
