<?php

/**
 * Simple SMTP EMail wrapper class. Nothing special.
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2007-2010
 * @license GPL
 * @version 1.0
 */

namespace sw;

class Mailer {

  /**
   * Global mailer template settings (set with setTemplate())
   *
   * @var mailer
   */
  private static $template = null;

  /**
   * Server connection ip/port
   *
   * @var string
   */
  private $server = "";

  /**
   * Login user name
   * @var string
   */
  private $user = "";

  /**
   * Logon password
   *
   * @var string
   */
  private $password = "";

  /**
   * Sender
   *
   * @var string
   */
  private $sender = null;

  /**
   * Recipient
   *
   * @var array
   */
  private $recipients = array();

  /**
   * eMail subject
   *
   * @var string
   */
  private $subject = "";

  /**
   * eMail body
   * @var string
   */
  private $body = "";

  /**
   * Content type
   * @var string
   */
  private $contentType = '';

  /**
   * The CC/BCC recipients
   * @var array
   */
  private $ccs = array();

  /**
   * Email verification
   * @param string $email
   * @return bool
   */
  public static function validateEmailAddress($email, $lookupDomain=false) {
    $email = explode('@', $email);
    if (count($email) != 2) {
      return false;
    } else {
      $domain = end($email);
      $email = reset($email);
      if (strlen(trim($domain)) < 2) {
        return false;
      } else if (strlen(trim($email)) < 1) {
        return false;
      } else if (strlen($email) > 64) {
        return false;
      } else if (strlen($domain) > 255) {
        return false;
      } else if (preg_match('/[!\.-]{2}/', $domain)) {
        return false;
      } else if (preg_match('/[!\.-]{2}/', $email)) {
        return false;
      } else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
        return false;
      } else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\", '', $email)) && !preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\", '', $email))) {
        return false;
      } else if ($lookupDomain && !(checkdnsrr($domain, "MX") || checkdnsrr($domain, "A"))) {
        return false;
      }
      return true;
    }
  }

  /**
   * Constructor
   */
  public function __construct() {
    
  }

  /**
   * Get server name/ip
   * @return string
   */
  public function getServer() {
    return $this->server;
  }

  /**
   * Sets server name and port
   * @param string $server
   */
  public function setServer($server) {
    $this->server = $server;
    settype($this->server, "string");
  }

  /**
   * Login user getter
   * @return string
   */
  public function getUser() {
    return $this->user;
  }

  /**
   * Sets the login user
   * @param string $user
   */
  public function setUser($user) {
    $this->user = $user;
    settype($this->user, "string");
  }

  /**
   * Login password getter
   * @return string
   */
  public function getPassword() {
    return $this->password;
  }

  /**
   * Login password setter
   * @param string $password
   */
  public function setPassword($password) {
    $this->password = $password;
    settype($this->password, "string");
  }

  /**
   * Add recipient, $cc can be 'CC' or 'BCC'
   * @param IEMailContact $contact
   * @param string $cc=''
   */
  public function addRecipient(IEMailContact $contact, $cc='') {
    $cc = empty($cc) ? '' : trim(strtolower($cc));
    if ($contact == null) {
      throw new LException("Recipient to add is not specified");
    } else if (!empty($cc) && $cc != 'cc' && $cc != 'bcc') {
      throw new LException('CC/BCC for Recipient to add must be specified with "CC" or "BCC"');
    } else {
      $this->recipients[] = $contact;
      if (!empty($cc)) {
        $this->ccs[$contact->toEMailAddressString()] = $cc;
      }
    }
  }

  /**
   * Recipient getter
   * @return IEMailContact[]
   */
  public function getRecipients() {
    return $this->recipients;
  }

  /**
   * Recipient getter with index
   * @return IEMailContact
   */
  public function getRecipient($index=0) {
    if (!array_key_exists($index, $this->recipients)) {
      throw new LException("Invalid index ':index'", array(':index' => $index));
    } else {
      return $this->recipients[$index];
    }
  }

  /**
   * Sender getter
   * @return IEMailContact
   */
  public function getSender() {
    return $this->sender;
  }

  /**
   * Sender setter
   * @param string $name
   */
  public function setSender(IEMailContact $contact) {
    if ($contact == null) {
      throw new LException("Argument is not an IEMailContact");
    } else {
      $this->sender = $contact;
    }
  }

  /**
   * eMail subject getter
   * @return string
   */
  public function getSubject() {
    return $this->subject;
  }

  /**
   * eMail subject setter
   * @param string $subject
   */
  public function setSubject($subject) {
    $this->subject = $subject;
    settype($this->subject, "string");
  }

  /**
   * eMail body getter
   * @return string
   */
  public function getBody() {
    return $this->body;
  }

  /**
   * eMail body setter
   * @param string $body
   */
  public function setBody($body) {
    $this->body = $body;
    settype($this->body, "string");
  }

  /**
   * Content type getter
   * @return string
   */
  public function getContentType() {
    return $this->contentType;
  }

  /**
   * Sets content type, only text/plain and text/html allowed.
   * @return void
   */
  public function setContentType($type) {
    $type = strtolower($type);
    if ($type != "text/plain" && $type != "text/html") {
      throw new LException("Only 'text/plain' or 'text/html' allowed");
    } else {
      $this->contentType = $type;
    }
  }

  /**
   * Use $this as default for all constructed mailers
   * @return void
   */
  public function setAsTemplate() {
    $ta = $this->recipients;
    $this->recipients = array();
    self::$template = clone $this;
    $this->recipients = $ta;
  }

  /**
   * Returns array with header, subject, body
   * @return array
   */
  public function getContents() {
    if (trim($this->getSubject()) == "") {
      throw new LException("No subject specified.");
    } else if (trim($this->getBody()) == "") {
      throw new LException("Empty body.");
    } else if (is_null($this->getSender())) {
      throw new LException("Sender not specified");
    } else if (count($this->getRecipients()) == 0) {
      throw new LException("No recipients defined.");
    } else {
      $from = $this->getSender()->toEMailAddressString();
      if (count($this->getRecipients()) < 2) {
        $to = $this->getRecipient()->toEMailAddressString();
        $cc = $bcc = '';
      } else {
        $to = $cc = $bcc = array();
        foreach ($this->getRecipients() as $recip) {
          $recip = $recip->toEMailAddressString();
          if (isset($this->ccs[$recip]) && $this->ccs[$recip] == 'cc') {
            $cc[] = $recip;
          } else if (isset($this->ccs[$recip]) && $this->ccs[$recip] == 'bcc') {
            $bcc[] = $recip;
          } else {
            $to[] = $recip;
          }
        }
        $to = implode(', ', $to);
        $cc = empty($cc) ? '' : 'Cc: ' . implode(', ', $cc) . "\r\n";
        $bcc = empty($bcc) ? '' : 'Bcc: ' . implode(', ', $bcc) . "\r\n";
      }

      if (empty($to)) {
        throw new LException('Email has no primary recipient (TO:)');
      } else {
        if (empty($this->contentType)) {
          $this->contentType = 'text/plain';
        }
        $header = "";
        $header .= "Content-Type: " . $this->getContentType() . "; charset=ISO-8859-15; format=flowed" . "\r\n";
        $header .= "Content-Transfer-Encoding: 8bit\r\n";
        $header .= "MIME-Version: 1.0" . "\r\n";
        $header .= "X-Accept-Language: de-de, de, en-us, en" . "\r\n";
        $header .= "From: $from\r\n";
        // To is handled in the mail function $header .= "To: $to\r\n";
        $header .= $cc . $bcc;
        $subject = $this->getSubject() . "\r\n";
        $content = array();
        $content["to"] = $to;
        $content["cc"] = $cc;
        $content["bcc"] = $bcc;
        $content["header"] = $header;
        $content["subject"] = $subject;
        $content["body"] = $this->getBody();
        return $content;
      }
    }
  }

  /**
   * Get email content to be sent
   * @return string
   */
  public function dump() { // getMailContent()
    $mail = $this->getContents();
    return $mail["header"] . "\n\n" . $mail["subject"] . "\n\n" . $mail["body"] . "\n";
  }

  /**
   * Sends the eMail
   * @return array succeeded posts
   */
  public function send() {
    $mail = $this->getContents();
    if (!mail($mail['to'], $mail["subject"], $mail["body"], $mail["header"])) {
      throw new LException("Could not send mail to recipients.");
    }
    Tracer::trace("EMail sent:\n" . print_r($mail, true) . "\n");
  }

}
