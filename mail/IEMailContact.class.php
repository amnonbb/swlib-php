<?php

/**
 * EMail contact interface, used in class Mailer and implemented in class EMailContact.
 * @gpackage de.atwillys.sw.php.swLib
 * @interface IEMailContact
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2006-2010
 * @license GPL
 * @version 1.0
 */

namespace sw;

interface IEMailContact { 
  
  /**
   * This is required to send an email
   * @return string
   */
  public function toEMailAddressString();
}
