<?php

/**
 * Math class for tasks that have to do with statistics and other Math stuff.
 * IMPORTANT: Take care with the variable types you use! PHP has functions that
 * return float instead of double. This is a big issue of this language. Try to
 * avoid type conversion from double to other types and back.
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2005-2010
 * @license GPL
 * @version 1.0
 * @uses MathException
 */

namespace sw;

class Math {

  /**
   * Calculates the sign/signum of a numeric value. The value zero is
   * positive: sign(10)=1, sign(0)=1, sign(-2)=-1.
   * @param double $value
   * return double
   */
  public static function sign($value) {
    if (!is_numeric($value)) {
      throw new MathException("Cannot calculate signum for non-numeric values (value=:value)", array(':value' => $value));
    } else {
      return $value < 0 ? -1 : 1;
    }
  }

  /**
   * Calculates the sign/signum of a numeric value. The value zero is
   * neither positive nor negative: sign(-2)=-1; sign(0)=0; sign(+2)=1;
   * @param double $value
   * return double
   */
  public static function signz($value) {
    if (!is_numeric($value)) {
      throw new MathException("Cannot calculate signum for non-numeric values (value=:value)", array(':value' => $value));
    } else {
      return $value < 0 ? -1 : ($value == 0 ? 0 : 1);
    }
  }

  /**
   * Calculates the factosials of a value
   * @param int $f
   * @return int
   */
  public static function factorial($f) {
    if (!is_numeric($f) || $f < 0) {
      throw new MathException("Cannot calculate factorials for numbers < 0 or no numbers (number=':f')", array(':f' => $f));
    } if ($f == 0) {
      return 0.0;
    } else {
      $r = 1;
      for ($i = 2; $i <= $f; $i++)
        $r *= $i;
      return $r;
    }
  }

  /**
   * Returns the mean value of a numeric array, empty cells (defined by an
   * empty string value "" are ignored (not assumed as 0))
   * @param array $values
   * @return double
   */
  public static function mean(array $values) {
    $n = count($values);
    if ($n == 0) {
      return 0;
    } else {
      $r = 0;
      foreach ($values as $v) {
        if (!is_numeric($v)) {
          if (trim($v) == '') {
            $n--;
          } else {
            throw new MathException("Cannot calculate mean value of a not numeric number (value=':v')", array(':v' => $v));
          }
        } else {
          $r += $v;
        }
      }
      return ($n > 0) ? ($r / $n) : 0;
    }
  }

  /**
   * Returns the variance value of a numeric array, empty cells (defined by an
   * empty string value "" are ignored (not assumed as 0))
   * @param array $values
   * @return double
   */
  public static function variance(array $values) {
    $n = count($values);
    if ($n < 2) {
      return 0;
    } else {
      $mean = self::mean($values);
      $r = 0;
      foreach ($values as $v) {
        if (!is_numeric($v)) {
          if (trim($v) == '') {
            $n--;
          } else {
            throw new MathException("Cannot calculate variance value of a not numeric number (value=':v')", array(':v' => $v));
          }
        } else {
          $r += ($v - $mean) * ($v - $mean);
        }
      }
      return $r / ($n - 1);
    }
  }

  /**
   * Returns the standard deviation of a numeric array, empty cells (defined
   * by an empty string value "" are ignored (not assumed as 0))
   * @param array $values
   * @return double
   */
  public static function std(array $values) {
    return sqrt(self::variance($values));
  }

}
