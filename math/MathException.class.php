<?php

/**
 * Exception thrown by the class Math. The EException throws this exceptions as
 * well in case of a "division by zero" warning.
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2007-2010
 * @license GPL
 * @version 1.0
 */

namespace sw;

class MathException extends LException {
  
}
