<?php

/**
 * Exception thrown by class ZipFile
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2007-2010
 * @license GPL
 * @version 1.0
 */

namespace sw;

class ZipException extends LException {
  
}
