<?php

/**
 * Json encoding/decoding
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2005-2010
 * @license GPL
 * @version 1.0
 */

namespace sw;

class Json {
  /**
   * Encode: Empty arrays shall be interpreted as empty objects
   */
  const ENCODE_EMPTY_ARRAYS_TO_OBJECTS = 1;

  /**
   * JSON string escaping
   * @param string $string
   * @return string
   */
  public static function escapeString($string) {
    $string = str_replace(array("\t", "\n", "\r", "\b", "\f", "\\", "/", "\""), array("\\t", "\\n", "\\r", "\\b", "\\f", "\\\\", "\\/", "\\\""), $string);
    mb_detect_encoding($string, "UTF-8") == "UTF-8" ? $string : utf8_encode($string);
    return $string;
  }

  /**
   * Converts an argument to a JSON string, if the data type is convertable.
   * The $maxDepth defines the maximum sub-array/sub-property to prevent
   * recursion. Check the class constants for the $flags.
   * @param mixed $data
   * @param int $flags
   * @param int $maxDepth
   * @return string
   */
  public static function encode($data, $flags=null, $maxDepth=null) {
    if (is_null($flags)) {
      $flags = 0;
    }
    if (false && defined('JSON_FORCE_OBJECT')) {
      $flags = 0 | (($flags & ENCODE_EMPTY_ARRAYS_TO_OBJECTS) ? JSON_FORCE_OBJECT : 0);
      return json_encode($data, $flags);
    }
    if (is_null($maxDepth) || $maxDepth <= 0) {
      $maxDepth = 64;
    }
    return self::encode_r($data, $flags, $maxDepth);
  }

  /**
   * Converts an argument to a JSON string, if the data type is convertable.
   * @param mixed $data
   * @param int $flags
   * @param int $level
   * @return string
   */
  public static function encode_r($data, $flags, $level) {
    if ($level < 0) {
      throw new LException('JSON encode failed due to exceeding the maximum (recursion) depth');
    }
    if (is_object($data)) {
      $data = get_object_vars($data);
      if (empty($data)) {
        return '{}';
      } else {
        foreach ($data as $key => $value) {
          $data[$key] = '"' . self::escapeString($key) . '":' . self::encode_r($value, $flags, $level - 1);
        }
        return '{' . implode(',', $data) . '}';
      }
    } else if (is_array($data)) {
      if (empty($data)) {
        return ($flags & self::ENCODE_EMPTY_ARRAYS_TO_OBJECTS) ? '{}' : '[]';
      } else {
        $isArray = true;
        foreach ($data as $key => $value)
          if (!is_numeric($key)) {
            $isArray = false;
            break;
          }
        if ($isArray) {
          foreach ($data as $key => $value) {
            $data[$key] = self::encode_r($value, $flags, $level - 1);
          }
          return '[' . implode(',', $data) . ']';
        } else {
          foreach ($data as $key => $value) {
            $data[$key] = '"' . self::escapeString($key) . '":' . self::encode_r($value, $flags, $level - 1);
          }
          return '{' . implode(',', $data) . '}';
        }
      }
    } else if (is_null($data)) {
      return 'null';
    } else if (is_bool($data)) {
      return $data ? 'true' : 'false';
    } else if (is_numeric($data)) {
      return strval($data);
    } else if (is_string($data)) {
      return '"' . self::escapeString($data) . '"';
    } else {
      throw new LException('Cannot convert type ":type" to JSON', array(':type' => gettype($data)));
    }
  }

  /**
   * Decodes a JSON string
   * @param string $json
   * @return mixed
   */
  public static function decode($json) {
    try {
      if (!is_scalar($json)) {
        throw new LException('Data is no scalar');
      } else {
        $data = @json_decode($json, true);
        if (is_null($data) || $data === false) {
          throw new LException('Function json_decode returned NULL');
        } else {
          return $data;
        }
      }
    } catch (\Exception $e) {
      throw new LException('JSON decode failed: :message', array(':message' => $e->getMessage()));
    }
  }

  /**
   * Reads and decodes a file
   * @param string $file 
   */
  public static function readFile($file) {
    return self::decode(FileSystem::readFile($file));
  }

  /**
   * Encodes and writes JSON to file
   * @param string $file
   * @param mixed $data 
   */
  public static function writeFile($file, $data, $flags=null, $maxDepth=null) {
    FileSystem::writeFile($file, self::encode($data, $flags, $maxDepth));
  }

}
