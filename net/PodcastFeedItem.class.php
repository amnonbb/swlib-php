<?php

/**
 * Provides rendering a podcast channel item into a RSS XML text.
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2011
 * @license GPL
 * @version 1.0
 */

namespace sw;

class PodcastFeedItem {

  // Feed item properties
  public $parentPodcast = null;
  public $title = '';
  public $description = '';
  public $pageLink = '';
  public $commentsPageLink = '';
  public $datePublished = '';
  public $author = '';
  public $authorEmail = '';
  public $category = '';
  public $keywords = '';
  public $duration = '';
  public $subtitle = '';
  public $explicit = false;
  public $block = false;
  public $fileUrl = '';
  public $fileLength = '';
  public $fileMimeType = '';

  /**
   *
   * @param type $path
   * @param type $parentPodcast
   */
  public function __construct($path='', $parentPodcast) {

    // Initialize by copying first
    if ($parentPodcast instanceof PodcastFeed) {
      $this->parentPodcast = $parentPodcast;
      $this->author = $parentPodcast->author;
      $this->authorEmail = $parentPodcast->authorEmail;
      $this->category = 'Podcast';
      $this->pageLink = '';
    }

    // Get infos from the file itself by reading the first part of the file using
    // a GET request.
    $path = trim($path);
    if (!empty($path)) {
      if (FileSystem::isFile($path)) {
        $ffmpeg = new FfmpegFile($path);
        $this->title = FileSystem::getBasename(FileSystem::getFileNameWithoutExtension($path)) . ' (' . date("Y-m-d h:i", FileSystem::getLastModified($path)) . ')';
        $this->fileUrl = $parentPodcast->urlRoot . '/' . trim(str_replace(array_keys($parentPodcast->fileToUrlTransformationRules) , array_values($parentPodcast->fileToUrlTransformationRules), $path), '/ ');
        $this->fileLength = FileSystem::getFileSize($path);
        $this->datePublished = FileSystem::getLastModified($path);
        $this->fileMimeType = mime_content_type($path);
        $this->duration = $ffmpeg->getDuration();
      }
    }
  }

  /**
   * Renders the feed item
   * @return string
   */
  public function compose() {
    return
      '<item>' . "\n"
      . ' <title>' . PodcastFeed::xmlEscape($this->title) . '</title>' . "\n"
      . ' <description>' . PodcastFeed::xmlEscape($this->description) . '</description>' . "\n"
      . (empty($this->pageLink) ? '' : (' <link>' . PodcastFeed::xmlEscape($this->pageLink) . '</link>' . "\n"))
      . (empty($this->commentsPageLink) ? '' : (' <comments>' . PodcastFeed::xmlEscape($this->commentsPageLink) . '</comments>' . "\n"))
      . ' <pubDate>' . PodcastFeed::xmlEscape(date(DATE_RFC1123, $this->datePublished)) . '</pubDate>' . "\n"
      . (empty($this->authorEmail) ? '' : (' <dc:creator>' . PodcastFeed::xmlEscape($this->authorEmail) . '</dc:creator>' . "\n"))
      . ' <category>' . PodcastFeed::xmlEscape($this->category) . '</category>' . "\n"
      . (empty($this->pageLink) ? '' : (' <guid isPermaLink="false">' . PodcastFeed::xmlEscape($this->pageLink) . '</guid>' . "\n"))
      . ' <content:encoded>' . PodcastFeed::xmlEscape($this->description) . '</content:encoded>' . "\n"
      . ' <itunes:duration>' . PodcastFeed::xmlEscape($this->duration) . '</itunes:duration>' . "\n"
      . ' <itunes:subtitle>' . PodcastFeed::xmlEscape($this->description) . '</itunes:subtitle>' . "\n"
      . ' <itunes:summary>' . PodcastFeed::xmlEscape($this->description) . '</itunes:summary>' . "\n"
      . ' <itunes:keywords>Podcast</itunes:keywords>' . "\n"
      . ' <itunes:author>' . PodcastFeed::xmlEscape($this->author) . '</itunes:author>' . "\n"
      . ' <itunes:explicit>' . ($this->explicit ? 'yes' : 'no') . '</itunes:explicit>' . "\n"
      . ' <itunes:block>' . ($this->block ? 'yes' : 'no') . '</itunes:block>' . "\n"
      . ' <enclosure url="' . dirname($this->fileUrl) . '/' . urlencode(basename($this->fileUrl)) . '" length="' . PodcastFeed::xmlEscapeAttribute($this->fileLength) . '" type="' . PodcastFeed::xmlEscapeAttribute($this->fileMimeType) . '" />' . "\n"
      . '</item>' . "\n"
    ;
  }

  /**
   * Returns the rendered xml version of the item
   * @return string
   */
  public function __toString() {
    return $this->compose();
  }


}
