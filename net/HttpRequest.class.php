<?php

/**
 * Http socket request wrapper
 * @gpackage de.atwillys.sw.php.swLib
 * @class HttpRequest
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2008-2010
 * @license GPL
 * @version 1.0
 */

namespace sw;

class HttpRequest {

  /**
   * The URL to connect to
   * @var string
   */
  private $uri = array();

  /**
   * Key-value assoc. array containing the post data
   * @var array
   */
  private $postData = '';

  /**
   * The port to connect to
   * @var int
   */
  private $port = 80;

  /**
   * Connection timeout in seconds
   * @var int
   */
  private $timeout = 30;

  /**
   * Get the content range header information
   * @var string
   */
  private $contentRange = '';

  /**
   * The user agent request header information
   * @var string
   */
  private $userAgent = '';

  /**
   * Contains the response
   * @var array
   */
  private $response = array();

  /**
   * Throw an exception if server response is 404 or the like
   * @var bool
   */
  private $throwExceptionOnErrorResponse = false;

  /**
   * Request headers, as key value array
   * @var array
   */
  private $requestHeaders = array();

  /**
   * Allow GZip encoding
   * @var bool
   */
  private $allowGzip = true;

  /**
   * Cookies as assoc array
   * @var array
   */
  private $cookies = array();

  /**
   * Sends a GET request and returns the response array
   * @param string $uri
   * @return array
   */
  public static function get($uri) {
    $o = new self();
    $o->setTimeout(60);
    return $o->request($uri)->getResponse();
  }

  /**
   * Constructor
   * @param string $uri
   * @param array $postData
   */
  public function __construct($uri=null, $postData=array()) {
    if (!is_null($uri))
      $this->setUri($uri);
    if (!is_null($postData))
      $this->setPostData($postData);
  }

  /**
   * Returns the URL to connect to
   * @return string
   */
  public function getUri() {
    return $this->uri;
  }

  /**
   * Sets the URL to connect to
   * @param string $uri
   * @return HttpRequest
   */
  public function setUri($uri) {
    $uri = trim($uri);
    if (empty($uri)) {
      throw new LException('URI is empty');
    } else {
      $this->uri = $uri;
    }
    return $this;
  }

  /**
   * Returns the port to connect to
   * @return int
   */
  public function getPort() {
    return $this->port;
  }

  /**
   * Sets the port to connect to
   * @param int $port
   */
  public function setPort($port) {
    if (!settype($port, 'integer')) {
      throw new LException('Port no integer: ":port"', array(':port' => $port));
    } else {
      $this->port = $port;
    }
    return $this;
  }

  /**
   * Returns the connection and stream timeout in seconds
   * @return int
   */
  public function getTimeout() {
    return $this->timeout;
  }

  /**
   * Sets the connection and stream timeout in seconds
   * @param int $seconds
   */
  public function setTimeout($seconds) {
    if (!settype($seconds, 'integer')) {
      throw new LException('Timeout no integer: ":seconds"', array(':seconds' => $seconds));
    } else {
      $this->timeout = $seconds > 0 ? $seconds : 0;
    }
    return $this;
  }

  /**
   * Sets the user agent string
   * @param string $agent
   * @return \sw\HttpRequest
   */
  public function setUserAgent($agent) {
    $this->userAgent = trim(strval($agent));
    return $this;
  }

  /**
   * Returns the user agent specification
   * @return string
   */
  public function getUserAgent() {
    return $this->userAgent;
  }

  /**
   * Returns the content range
   * @return string
   */
  public function getContentRange() {
    return $this->contentRange;
  }

  /**
   * Sets the content range to request. Can be an array($start, $end) or a string
   * "start-end".
   * @param mixed $range
   */
  public function setContentRange($range) {
    if (empty($range)) {
      $this->contentRange = '';
    } else if (is_array($range)) {
      if (count($range) != 2 || !is_numeric($range[0]) || !is_numeric($range[1])) {
        throw new LException("Range array setting is incorrect.");
      }
    } else if (is_numeric($range) && $range >= 0) {
      $range = intval($range);
      $this->contentRange = "0-$range";
    } else if (is_scalar($range)) {
      $range = trim($range);
      if (empty($range)) {
        $this->contentRange = '';
      } else {
        $range = explode('-', $range);
        $range[0] = trim($range[0]);
        $range[1] = trim($range[1]);
        if (!is_numeric($range[0]) || $range[0] < 0 || !is_numeric($range[1]) || $range[1] < 0) {
          throw new LException("Content range setting is wrong must be in format start-end, e.g. 0-4095");
        } else {
          $range[0] = intval($range[0]);
          $range[1] = intval($range[1]);
          $this->contentRange = implode('-', $range);
        }
      }
    }
    return $this;
  }

  /**
   * Returns the post data key-value array
   * @return array
   */
  public function getPostData() {
    return $this->postData;
  }

  /**
   * Sets the post data, if empty, the request will be GET instead of POST
   * @param array $postData
   * @return HttpRequest
   */
  public function setPostData($postData) {
    if (empty($postData)) {
      $this->postData = array();
    } else if (!is_array($postData)) {
      throw new LException("Post data must be an array");
    } else {
      $this->postData = $postData;
    }
    return $this;
  }

  /**
   * Returns the request headers as assoc array.
   * @return array
   */
  public function getRequestHeaders() {
    return $this->requestHeaders;
  }

  /**
   * Sets the request headers
   * @param array $headers
   * @param HttpRequest
   */
  public function setRequestHeaders(array $headers=array()) {
    $this->requestHeaders = array();
    foreach ($headers as $k => $v) {
      $this->requestHeaders[trim($k, "\t\r\n ")] = trim($v, "\t\r\n ");
    }
    return $this;
  }

  /**
   * Set if an exception shall be thrown by request() if the server
   * responds with an error message, such as 404
   * @param bool $throw
   * @return HttpRequest
   */
  public function setThrowExceptionOnErrorResponse($throw) {
    $this->throwExceptionOnErrorResponse = (bool) $throw;
    return $this;
  }

  /**
   * Returns if an exception shall be thrown by request() if the server
   * responds with an error message, such as 404
   * @return bool
   */
  public function getThrowExceptionOnErrorResponse() {
    return $this->throwExceptionOnErrorResponse;
  }

  /**
   * Returns the associative response array
   * @return array
   */
  public function getResponse() {
    return $this->response;
  }

  /**
   * Returns if the request was successful
   * @return bool
   */
  public function isResponseOk() {
    return floor($this->response['status'] / 100) == 2 || in_array($this->response['status'], array(302, 307));
  }

  /**
   * Returns the server response status code.
   * (200=OK ...)
   * @return int
   */
  public function getResponseStatus() {
    return $this->response['status'];
  }

  /**
   * Returns an associative array containing the
   * response headers.
   * @return array
   */
  public function getResponseHeaders() {
    return $this->response['headers'];
  }

  /**
   * Returns a string containing the response content/document/body
   * @return string
   */
  public function getResponseBody() {
    return $this->response['content'];
  }

  /**
   *
   */
  public function setRequestCookies(array $cookies) {
    $this->cookies = $cookies;
    return $this;
  }

  /**
   * Start the request, parse the response.
   * You can also specify the URL by using
   * setURI(...) instead of using the optional
   * parameter $uri.
   * @param string $uri
   * @param int $port
   * @return HttpRequest
   */
  public function request($uri=null, $port=null) {

    // Init
    $this->response = array(
      'status' => 0,
      'status-text' => '',
      'headers' => array(),
      'content' => '',
    );

    if (!empty($uri)) {
      $this->setURI($uri);
    }

    if (!empty($port)) {
      $this->setPort($port);
    }

    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_HEADER => true,
      CURLOPT_URL => $this->uri,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_CONNECTTIMEOUT => $this->timeout,
      CURLOPT_TIMEOUT => $this->timeout,
      CURLOPT_FILETIME => true,
      CURLOPT_FORBID_REUSE => true,
      CURLOPT_NOPROGRESS => true,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_MAXREDIRS => 10,
    ));

    if (!empty($this->cookies)) {
      $cookies = array();
      foreach ($this->cookies as $k => $v) {
        $cookies[] = urlencode($k) . '=' . urlencode($v);
      }
      $this->requestHeaders['Cookie'] = implode('; ', $cookies);
    }
    if (!empty($this->requestHeaders) && is_array($this->requestHeaders)) {
      $headers = array();
      foreach ($this->requestHeaders as $k => $v) {
        $headers[] = "$k: $v";
      }
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    }
    if (!empty($this->contentRange)) {
      curl_setopt($curl, CURLOPT_RANGE, $this->contentRange);
    }
    if (!empty($this->userAgent)) {
      curl_setopt($curl, CURLOPT_USERAGENT, $this->userAgent);
    }
    if (!empty($this->allowGzip)) {
      curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
    }
    if (!empty($this->postData)) {
      curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($this->postData));
    }

    $response = curl_exec($curl);
    curl_close($curl);
    if ($response === false) {
      throw new LException("Http request failed:", curl_error($curl));
    }

    $headers = $cookies = array();
    $status = -1;
    while (strpos($response, "\n") !== false) {
      list($r, $response) = explode("\n", $response, 2);
      $r = trim($r, "\r\t ");
      if (strlen($r) === 0) {
        break;
      } else if ($status == -1) {
        $r = explode(' ', $r, 3);
        $status = intval($r[1]);
        $statusText = trim($r[2]);
      } else {
        $r = explode(':', $r, 2);
        $r[0] = trim(strtolower($r[0]));
        $r[1] = trim($r[1]);
        if ($r[0] == 'set-cookie') {
          $r = explode('=', reset(explode(';', $r[1], 2)), 2);
          if (count($r) == 2) {
            $cookies[$r[0]] = urlencode($r[1]);
          } else {
            $cookies[$r[0]] = '';
          }
        } else {
          $headers[$r[0]] = empty($r[1]) ? '' : trim($r[1]);
        }
      }
    }

    if (count($headers) == 0) {
      throw new LException("Did not receive a valid response (response headers missing)");
    }

    // Build response
    $this->response = array(
      'status' => $status,
      'status-text' => $statusText,
      'headers' => $headers,
      'cookies' => $cookies,
      'content' => $response,
    );

    if ($this->throwExceptionOnErrorResponse && !$this->isResponseOk()) {
      throw new LException("HTTP request failed: $statusText", $status);
    }
    return $this;
  }

}
