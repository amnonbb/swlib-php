<?php

/**
 * Automatic podcast generator.
 *
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2008-2010
 * @license GPL
 */

namespace sw;

class AutoPodcast {

  public $rootUri = null;
  public $sourceDirectory = null;
  public $cacheDirectory = null;
  public $logLevel = 0;
  public $logFile = null;
  public $defaultIcon = null;
  public $castListPageView = null;
  public $defaultMediaFilePattern = '*.mp4'; // *.{mp4,mp3,aac,m4a,m4v,pdf,epub}';
  private $castList = null;
  private $sourceDirs = null;

  /**
   *
   */
  public function __construct() {
    ;
  }

  /**
   * Returns the media directory of a podcast that is linked/registered in
   * the source directory.
   * @param string $cast
   * @return string
   */
  public function getSource($cast) {
    if ($this->sourceDirs === null) {
      $dirs = array();
      foreach (glob($this->sourceDirectory . "/*") as $link) {
        $dir = null;
        if (is_link($link)) {
          $dir = @readlink($link);
        } else if (is_file($link)) {
          $dir = trim(@file_get_contents($link), "\r\n\t ");
        } else {
          continue;
        }
        $dirs[strtolower(basename($link))] = empty($dir) ? null : ($dir);
      }
      $this->sourceDirs = $dirs;
    }
    $cast = strtolower($cast);
    return isset($this->sourceDirs[$cast]) ? $this->sourceDirs[$cast] : null;
  }

  /**
   * Returns the details about all sources
   * @return array
   */
  public function getCasts() {
    if (empty($this->castList)) {
      if (!is_file($this->cacheDirectory . '/castlist.json')) {
        try {
          $this->update();
        } catch (\Exception $e) {
          return array();
        }
      }
      $this->castList = json_decode(file_get_contents($this->cacheDirectory . '/castlist.json'), true);
      if (empty($this->castList))
        $this->castList = array();
    }
    return $this->castList;
  }

  /**
   * Returns a detailed list of a podcasts episodes
   * @param string $cast
   */
  public function getEpisodeList($cast) {
    $dir = $this->getCasts();
    $episodes = array();
    if (!isset($dir[strtolower($cast)]))
      return array();
    $cast = $dir[strtolower($cast)];
    $files = glob($cast['cache'] . '/*.json');
    $times = array();
    foreach ($files as $file)
      $times[$file] = filemtime($file);
    asort($times, SORT_DESC);
    $files = array_keys($times);
    unset($times);
    foreach ($files as $file) {
      $ep = @json_decode(@file_get_contents($file), true);
      if (is_array($ep)) {
        unset($ep['local-file']);
        $ep['uri'] = $cast['url'] . $ep['path']; // rawurlencode($ep['path'])
        unset($ep['path']);
      }
      $episodes[] = $ep;
    }
    return $episodes;
  }

  /**
   * Retrieves metadata from a video file into an assoc array. Required keys
   * are:
   *  title: string
   *  description: string
   *  comment: string
   *  genre: string
   *  created: timestamp
   *
   * @param string $file
   * @return array
   */
  public function readVideoFileInfo($file) {
    $meta_data = array();
    try {
      $ffmpeg = new FfmpegFile($file);
      try {
        $meta_data['title'] = trim($ffmpeg->getContainerMetaData('title'));
      } catch (\Exception $e) {
        ;
      }
      if (empty($meta_data['title'])) {
        $meta_data['title'] = trim(FileSystem::getFileNameWithoutExtension(FileSystem::getBasename($file)));
      }
      try {
        $meta_data['description'] = trim($ffmpeg->getMetadata('description'));
      } catch (\Exception $e) {
        ;
      }
      try {
        $meta_data['comment'] = trim($ffmpeg->getMetadata('comment'));
      } catch (\Exception $e) {
        ;
      }
      try {
        $meta_data['genre'] = trim($ffmpeg->getMetadata('genre'));
      } catch (\Exception $e) {
        ;
      }
      try {
        if (($t = @strtotime(trim($ffmpeg->getMetadata('date')))) !== false) {
          $meta_data['created'] = $t;
        } else if (($t = @strtotime(trim($ffmpeg->getMetadata('creation_time')))) !== false) {
          $meta_data['created'] = $t;
        } else {
          $meta_data['created'] = filemtime($file);
        }
      } catch (\Exception $e) {
        ;
      }
    } catch(\Exception $e) {
      Tracer::trace("Failed to read video file '$file': $e");
    }
    return $meta_data;
  }

  /**
   *
   * @param type $file
   * @return type
   */
  public function readAudioFileInfo($file) {
    return $this->readVideoFileInfo($file); // not yet implemented, use ffmpeg
  }

  /**
   *
   * @param type $file
   * @return type
   */
  public function readDocumentFileInfo($file) {
    return array(); // not yet implemented
  }

  /**
   *
   * @param string $cast
   */
  public function checkUpdate($cast) {
    // nothing yet
  }

  /**
   * Sets, resets or returns the status of the update lock.
   * If null or no argument, returns the current lock state: true=locked.
   * If argument is true/false returns true on success, false on error (means
   * already locked by another script process).
   * @param bool $set_lock=null
   */
  public function lockUpdate($set_lock = null) {
    $lockfile = $this->cacheDirectory . '/.update-lock';
    $locked = (is_file($lockfile) && time() < filemtime($lockfile) + 600);
    if ($set_lock === null) {
      return $locked;
    } else if (empty($set_lock)) {
      @unlink($lockfile);
      return true;
    } else if (!$locked) {
      @file_put_contents($lockfile, time());
      return true;
    }
    return false;
  }

  /**
   * Updates one or all podcasts
   * @param string $cast_to_update
   * @param mixed $force_update = false
   */
  public function update($cast_to_update = null, $force_update = false) {

    // Be independent from the host connection and lock the update process
    if (!$this->lockUpdate(true)) {
      throw new Exception("Cache is being processed, try later.");
    }

    try {
      @ignore_user_abort();
      Tracer::trace('+++ CAST LIST UPDATE +++');

      // Check if the castlist file exists or a complete rebuild is required
      $this->castList = json_decode(file_get_contents($this->cacheDirectory . '/castlist.json'), true);
      if (empty($this->castList)) {
        $cast_to_update = null;
        $this->castList = array();
      }

      // Scan cast list for changes
      foreach (glob($this->sourceDirectory . "/*") as $source_link) {
        if (!is_file($source_link) && !is_link($source_link))
          continue;
        $cast_name = basename($source_link);
        if ($cast_to_update !== null && strtolower($cast_to_update) != strtolower($cast_name)) {
          continue;
        }
        $this->castList[trim(strtolower($cast_name))] = null;
        $source_dir = $this->getSource($cast_name);
        if (!is_readable($source_dir)) {
          Tracer::trace("Update: Skipped, source directory not readable: '$source_dir' (for link '$source_link')");
          continue;
        } else if (!is_dir($source_dir)) {
          Tracer::trace("Update: Skipped, source directory link target is no directory: '$source_dir' (for link '$source_link')");
          continue;
        }

        // Get meta information about the actual scanned cast
        $actual_cast = array('name' => '', 'dir' => '', 'url' => '', 'title' => '',
            'description' => '', 'icon' => '', 'icon-file' => '', 'author' => '',
            'author-email' => '', 'cache' => '');

        $actual_cast['name'] = $cast_name;
        $actual_cast['dir'] = $source_dir;
        $actual_cast['url'] = rtrim($this->rootUri, '/') . "/" . $cast_name;
        $actual_cast['icon'] = $actual_cast['url'] . '.png';
        $actual_cast['cache'] = $this->cacheDirectory . "/" . $cast_name;
        $actual_cast['last-modified'] = time();

        // Meta data saved in the .autocast directory located where the media files are.
        $info_dir = FileSystem::getOneOf(array("$source_dir/autocast", "$source_dir/.autocast"), false);
        if (is_dir($info_dir)) {
          Tracer::trace("Cast meta directory found: $info_dir", 1);
          $fn = FileSystem::getOneOf(array("$info_dir/title*", "$info_dir/name*"), false);
          $actual_cast['title'] = empty($fn) ? '' : trim(@file_get_contents($fn), "\n\r\t ");

          $fn = FileSystem::getOneOf(array("$info_dir/description*"), false);
          $actual_cast['description'] = empty($fn) ? '' : trim(@file_get_contents($fn), "\n\r\t ");

          $fn = FileSystem::getOneOf(array("$info_dir/author", "$info_dir/author.*"), false);
          $actual_cast['author'] = empty($fn) ? '' : trim(@file_get_contents($fn), "\n\r\t ");

          $fn = FileSystem::getOneOf(array("$info_dir/author-email*"), false);
          $actual_cast['author-email'] = empty($fn) ? '' : trim(@file_get_contents($fn), "\n\r\t ");

          $fn = FileSystem::getOneOf(array("$info_dir/filepattern*"), false);
          $actual_cast['filepattern'] = empty($fn) ? '' : trim(@file_get_contents($fn), "\n\r\t ");

          if (@count($r = @glob("$info_dir/*.png")) > 0)
            $actual_cast['icon-file'] = @reset($r);
        }
        if (empty($actual_cast['filepattern'])) {
          $actual_cast['filepattern'] = $this->defaultMediaFilePattern;
        }
        $this->castList[trim(strtolower($cast_name))] = $actual_cast;
        @file_put_contents($this->cacheDirectory . '/castlist.json', json_encode($this->castList, JSON_FORCE_OBJECT));

        Tracer::trace_r($this->castList, '$this->castList');

        // Scan the media files of the actually updated cast
        try {
          $cast_name = $actual_cast['name'];
          $source_dir = $actual_cast['dir'];
          $cast_cache_dir = $this->cacheDirectory . "/" . $cast_name;
          if ($cast_to_update !== null && strtolower($cast_to_update) != strtolower($cast_name)) {
            continue;
          }

          // Find media files
          $files = FileSystem::find($source_dir, $actual_cast['filepattern'], '', true);
          $times = array();
          foreach ($files as $file) {
            $times[$file] = filemtime($file);
          }
          asort($times, SORT_DESC);
          $files = array_keys($times);
          unset($times);

          // Update cast files
          if (!is_dir("$cast_cache_dir")) {
            FileSystem::mkdir("$cast_cache_dir", 0777);
          }
          $cast_has_changed = $force_update ? true : false;
          $cast_items = array();
          foreach ($files as $file) {
            try {
              $info_file = "$cast_cache_dir/" . trim(str_replace('/', '-', str_replace($source_dir, '', $file)), '-') . '.json';
              $meta_data = null;
              if (is_file($info_file) && filemtime($info_file) >= filemtime($file)) {
                $meta_data = @json_decode(@file_get_contents($info_file), true);
              }
              if (!is_array($meta_data) || empty($meta_data)) {
                $cast_has_changed = true;
                switch (strtolower(FileSystem::getExtension($file))) {
                  case 'mp4':
                  case 'm4v':
                  case 'mov':
                    $meta_data = $this->readVideoFileInfo($file);
                    break;
                  case 'mp3':
                  case 'm4a':
                  case 'aac':
                    $meta_data = $this->readAudioFileInfo($file);
                    break;
                  case 'pdf':
                  case 'epub':
                    $meta_data = $this->readDocumentFileInfo($file);
                    break;
                }
                $meta_data = array_merge(array('title' => '', 'description' => '', 'comment' => '', 'genre' => '', 'created' => 0), $meta_data);
                $meta_data['category'] = 'TV & Film';
                $meta_data['path'] = str_replace($source_dir, '', $file);
                $meta_data['local-file'] = $file;
                $meta_data['created'] = filemtime($file);
                FileSystem::writeFile($info_file, json_encode($meta_data, JSON_FORCE_OBJECT));
                FileSystem::chmod($info_file, 0666);
                FileSystem::touch($info_file, filemtime($file));

                Tracer::trace("Update: Cached $info_file", 2);
                if (!empty($meta_data['title']))
                  Tracer::trace("-- Title: {$meta_data['title']}", 2);
                if (!empty($meta_data['description']))
                  Tracer::trace("-- Description: {$meta_data['description']}", 2);
                if (!empty($meta_data['comment']))
                  Tracer::trace("-- Comment: {$meta_data['comment']}", 2);
                if (!empty($meta_data['genre']))
                  Tracer::trace("-- Genre: {$meta_data['genre']}", 2);
                if (!empty($meta_data['created']))
                  Tracer::trace("-- Created on: {$meta_data['created']}", 2);
              }
              $cast_items[] = $meta_data;
            } catch(\Exception $e) {
              Tracer::traceException($e);
            }
          }

          if ($cast_has_changed) {
            Tracer::trace("Update: Feed update required for '$cast_name'", 2);            
            
            // Copy icon to cache
            if (is_file($actual_cast['icon-file']) && strtolower(FileSystem::getExtension($actual_cast['icon-file'])) == 'png') {
              FileSystem::copy($actual_cast['icon-file'], $this->cacheDirectory . '/' . $cast_name . '.png');
              FileSystem::chmod($this->cacheDirectory . '/' . $cast_name . '.png', 0666);
            }

            // Regenerate and write XML
            $feed = new PodcastFeed(
              rtrim($this->rootUri, '/') . "/" . urlencode($cast_name), empty($actual_cast['title']) ? $actual_cast['name'] : $actual_cast['title'], $actual_cast['description'], $actual_cast['author'], $actual_cast['author-email'], array($source_dir => '') // directory mapping
            );
            $feed->updatePeriodTtlInS = 120; // 2h
            try {
              $feed->setImage($actual_cast['icon']);
            } catch (\Exception $e) {
              Tracer::trace("Update: Icon of '$cast_name' not ok: " . $e->getMessage());
            }
            foreach ($cast_items as $it) {
              set_time_limit(120);
              try {
                $item = $feed->addItem($it['local-file']);
                $item->title = $it['title'];
                $item->description = $it['description'];
                $item->pageLink = $it['comment'];
                $item->category = $it['category'];
                $item->datePublished = $it['created'];
              } catch (\Exception $e) {
                Tracer::traceException($e);
              }
            }
            FileSystem::writeFile($this->cacheDirectory . '/' . $cast_name . '.xml', $feed);
            FileSystem::chmod($this->cacheDirectory . '/' . $cast_name . '.xml', 0666);
          }
        } catch (Exception $e) {
          Tracer::traceException($e);
        }
      }
    } catch (\Exception $e) {
      $this->lockUpdate(false);
      throw $e;
    }
    $this->lockUpdate(false);
  }

  /**
   * Cleares the entire cache
   * @return bool
   */
  public function clearCache() {
    $ex = null;
    if (!$this->lockUpdate(true)) {
      throw new Exception("Cache is being processed, try later.");
    }
    foreach (glob($this->cacheDirectory . '/*') as $file) {
      if ($file != $this->logFile) {
        try {
          FileSystem::delete($file);
        } catch (Exception $e) {
          Tracer::trace("Clear cache: Failed to delete '$file': " . $e->getMessage());
          $ex = $e;
        }
      }
    }
    $this->lockUpdate(false);
    if ($ex !== null)
      throw $ex;
  }

  /**
   * Sends the list of casts
   * @return void
   */
  protected function sendList() {
    $casts = $this->getCasts();
    header('Content-Type: text/html; Charset=UTF-8', true);
    $template = $this->castListPageView;
    try {
      if (is_callable($template)) {
        $this->castListPageView($casts);
      } else if (is_file($template) && is_readable($template)) {
        @chdir(dirname($template));
        @include $template;
      } else {
        print '<!doctype html><html><head><title>[Hades] autocast</title></head><body><h1 class="podcast-list">Podcastcast list</h1><ul id="podcast-list">' . "\n";
        foreach ($casts as $cast) {
          print "<li><a href=\"" . rtrim($this->rootUri, '/') . "/" . rawurlencode($cast['name']) . "/\">" . htmlspecialchars($cast['title']) . "</a></li>\n";
        }
        print "</ul></body></html>\n";
      }
    } catch (\Exception $e) {
      print "Error in the list view template: $e\n";
    }
  }

  /**
   * Sends a podcast XML feed. Auto-updates before sending.
   * @param string $cast
   * @return void
   */
  protected function sendFeed($cast) {
    $this->checkUpdate($cast);
    foreach (glob($this->cacheDirectory . '/*.xml') as $file) {
      if (stripos($cast, FileSystem::getFileNameWithoutExtension(FileSystem::getBasename($file))) !== false) {
        @header('Content-Type:  application/rss+xml; Charset=UTF-8');
        print FileSystem::readFile($file);
        return;
      }
    }
    header('Content-Type: text/plain; Charset=UTF-8', true, 404);
    print "404 Not found\n";
  }

  /**
   * Sends a podcast episode media file
   * @param string $cast
   * @param string $path
   */
  protected function sendItem($cast, $path) {
    $dir = $this->getSource($cast);
    $file = "$dir/" . trim($path, '/ ');
    if (!$dir) {
      header('Content-Type: text/plain; Charset=UTF-8', true, 404);
      print "Podcast not found\n";
    } else if (!is_file($file)) {
      header('Content-Type: text/plain; Charset=UTF-8', true, 404);
      print "Podcast media file not found\n";
    } else if (!is_readable($file)) {
      header('Content-Type: text/plain; Charset=UTF-8', true, 501);
      print "Podcast media file not readable\n";
    } else {
      Tracer::disable();
      $dl = new ResourceFile($file);
      $dl->download();
      exit();
    }
  }

  /**
   * Sends a cast icon (png file) or if not existing the global autocast icon.
   * @param string $cast
   */
  protected function sendIcon($cast) {
    $dl = $this->defaultIcon;
    foreach (@glob($this->cacheDirectory . '/*.png') as $file) {
      if (stripos($file, "$cast.png") !== false) {
        $dl = $file;
        break;
      }
    }
    if (!is_file($dl)) {
      header('Content-Type: text/plain; Charset=UTF-8', true, 404);
      print "Podcast icon not found.";
    } else {
      $rc = new ResourceFile($dl);
      $rc->download();
      exit(0);
    }
  }

  /**
   * Sends the list of episodes in JSON
   * @param string $cast
   */
  protected function sendEpisodeList($cast) {
    print @json_encode($this->getEpisodeList($cast));
  }

  /**
   * Main function
   * @return void
   */
  public function main() {
    try {
      Tracer::setLevel($this->logLevel);
      Tracer::appendProtocol(false);
      header('Content-Type: text/plain; Charset=UTF-8', true);
      Tracer::trace("Request: {$_SERVER['REQUEST_URI']}\n");

      $request = $_SERVER['REQUEST_URI'];

      if (strpos($request, '?') !== false) {
        $request = explode('?', $request, 2);
        $request = @reset($request);
      }
      $request = rtrim($request, '/');
      if (stripos($request, $_SERVER['SCRIPT_NAME']) === 0) {
        if(strlen($request) == strlen($_SERVER['SCRIPT_NAME'])) {
          $request = '';
        } else {
          $request = substr($request, strlen($_SERVER['SCRIPT_NAME']));
        }        
      } else if (stripos($request, dirname($_SERVER['SCRIPT_NAME'])) === 0) {
        if(strlen($request) == strlen(dirname($_SERVER['SCRIPT_NAME']))) {
          $request = '';
        } else {
          $request = substr($request, strlen(dirname($_SERVER['SCRIPT_NAME'])));
        }        
      }      
      $request = array_filter(explode('/', urldecode(ltrim($request, '/')), 2));

      if (empty($request)) {
        if (!empty($_GET)) {
          reset($_GET);
          header('Content-Type: text/plain; Charset=UTF-8', true);
          $command = strtolower(trim(key($_GET)));
          try {
            switch ($command) {
              case 'log':
                print @file_get_contents($this->logFile);
                break;
              case 'log-reset':
              case 'logreset':
              case 'reset-log':
              case 'resetlog':
                @file_put_contents($this->logFile, '');
                print "Log file deleted.\n";
                break;
              case 'clear':
              case "clear-cache":
                $this->clearCache();
                print "Cache cleared\n";
                break;
              case 'reset':
              case 'rebuild':
                $this->clearCache();
              case 'update':
              case 'build':
                @file_put_contents($this->logFile, '');
                $this->update();
                break;
              default:
                header('Not found', true, 404);
                print "Command '$command' not found.\n";
            }
          } catch (\Exception $e) {
            print $e->getMessage() . "\n";
          }
        } else {
          $this->sendList();
        }
      } else if (count($request) == 1 && stripos(reset($request), '.png') !== false) {
        $this->sendIcon(FileSystem::getFileNameWithoutExtension(reset($request)));
      } else if (count($request) == 1) {
        $this->sendFeed(reset($request));
      } else {
        if (trim(end($request), ' /') == 'episodes') {
          $this->sendEpisodeList(reset($request));
        } else {
          $this->sendItem(reset($request), end($request));
        }
      }
    } catch (\Exception $e) {
      Tracer::traceException($e);
      header('Content-Type: text/plain; Charset=UTF-8', true);
      print "Ooops, that should not have happened!\n\nWe've got an exception here: $e\n";
    }
    try {
      $trace = trim(Tracer::disable(true), "\n\r\t ");
      if (!empty($trace)) {
        @file_put_contents($this->logFile, $trace . "\n", FILE_APPEND | LOCK_EX);
        try {
          FileSystem::chmod($this->logFile, 0666);
        } catch (\Exception $e) {
        }
      }
    } catch (\Exception $e) {
      ;
    }
  }
}
