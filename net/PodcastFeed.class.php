<?php

/**
 * Provides rendering one podcast channel into a RSS XML text. Assumed is that
 * there is only one channel in the feed.
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2011
 * @license GPL
 * @version 1.0
 */

namespace sw;

class PodcastFeed {

  /**
   * Title of the podcast
   * @var string
   */
  public $title = '';

  /**
   * Description/summary of the podcast
   * @var string
   */
  public $description = '';

  /**
   * Link to the main HTML (info/summary etc) page of the cast
   * @var string
   */
  public $pageLink = '';

  /**
   * Link to a page where people can leave comments about the cast
   * @var string
   */
  public $commentsPageLink = '';

  /**
   * The name of the podcast manager/author
   * @var string
   */
  public $author = '';

  /**
   * The email address of the author
   * @var string
   */
  public $authorEmail = '';

  /**
   * Category information in the RSS feed
   * @var string
   */
  public $category = '';

  /**
   * Keywords in the feed, set the values comma separated
   * @var string
   */
  public $keywords = '';

  /**
   * Date when the podcast feed was last actualized. Automatically set in the
   * constructor.
   * @var string
   */
  public $lastBuildDate = '';

  /**
   * Language hint in the podcast RSS
   * @var string
   */
  public $language = '';

  /**
   * Copyright info in the podcast RSS
   * @var string
   */
  public $copyright = '';

  /**
   * Time to live in minutes. This is an info for the client program how frequently
   * the cast shall be polled.
   * @var int
   */
  public $updatePeriodTtlInS = 1440;

  /**
   * Link to the cover image. Set it using when setImage()
   * @var int
   */
  public $imageLink = '';

  /**
   * Width of the cover image. Automatically set when setImage() is called
   * @var int
   */
  public $imageWidth = 0;

  /**
   * Height of the cover image. Automatically set when setImage() is called
   * @var int
   */
  public $imageHeight = 0;

  /**
   *
   * @var bool
   */
  public $block = false;

  /**
   * Describes if the podcast has to be loaded explicitly (instead of automaticly)
   * @var bool
   */
  public $explicit = false;

  /**
   * The root uri of the feed, e.g. "http://www.example.org".
   * @var string
   */
  public $urlRoot = '';

  /**
   * An assoc array defining how local media files are mapped to urls. Useful
   * if files are stored in a directory that is not in the $_SERVER["DOCUMENT_ROOT"],
   * but made accessible for HTTP using web server configurations. If empty,
   * the feed item media file locations will assumed to be in a sub directory of
   * the DOCUMENT_ROOT, which will be removed from the string to retain
   * the file URI. E.g.
   *
   * $feed->fileToUrlTransformationRules = array(
   *    // PATTERN
   *    'LOCAL PATH' => 'URL PREFIX WITHOUT THE URL ROOT',
   *
   *    // Standard behavior
   *    $_SERVER['DOCUMENT_ROOT'] => '',
   *
   *    // E.g. in apache2.conf the directory media of mounted diskN was made
   *    // available as https://localhost/media
   *    '/mnt/diskN/media' => '/media'
   * );
   *
   * @var array
   */
  public $fileToUrlTransformationRules = array();

  /**
   * Contains the channel items
   * @var PodcastFeedItem[]
   */
  public $items = array();

  /**
   * Escapes a string, so that it is compliant to XML node content texts. Preferred
   * escapint is <![CDATA[...]]>, if this is not possible, XML special chaar
   * escaping is used.
   * @param string $text
   * @return string
   */
  public static function xmlEscape($text) {
    if (empty($text) || trim($text) == '') { // !empty for array()/null
      return '';
    } else if (!preg_match('/([^\x01-\x7f]|[&<>])/', $text)) {
      return $text;
    } if (strpos($text, ']]>') === false) {
      $text = mb_convert_encoding($text, 'UTF-8', 'auto');
      return "<![CDATA[$text]]>";
    } else {
      return str_replace(array('&', '"', "'", '<', '>'), array('&amp;', '&quot;', '&apos;', '&lt;', '&gt;'), $text);
    }
  }

  /**
   * Escapes a text so that it is compliant to XML attribute values
   * @param string $text
   * @return string
   */
  public static function xmlEscapeAttribute($text) {
    $text = mb_convert_encoding($text, 'UTF-8', 'auto');
    return htmlspecialchars($text, ENT_COMPAT | ENT_XML1, 'UTF-8');
  }

  /**
   * Constructor
   */
  public function __construct($baseUri=null, $title='', $description='', $author='', $authorEmail='', $fileToUrlTransformationRules=array()) {
    if ($baseUri === null) {
      $this->urlRoot = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/';
    } else {
      $this->urlRoot = $baseUri;
    }

    if(empty($fileToUrlTransformationRules) || !is_array($fileToUrlTransformationRules)) {
      $this->fileToUrlTransformationRules = array(
          $_SERVER['DOCUMENT_ROOT'] => ''
      );
    } else {
      $this->fileToUrlTransformationRules = $fileToUrlTransformationRules;
    }

    $this->urlRoot = rtrim($this->urlRoot, '/ ');
    $this->title = trim($title);
    $this->description = trim($description);
    $this->author = trim($author);
    $this->authorEmail = trim($authorEmail);
    $this->lastBuildDate = date(DATE_RSS, time());
    $this->language = 'en-en';
    $this->pageLink = $this->urlRoot . '/';
    //$this->authorEmail =
  }

  /**
   * Sets the image file and reads the image dimensions
   * @param string $link
   */
  public function setImage($link) {
    if (empty($link)) {
      $this->imageLink = '';
      $this->imageHeight = '';
      $this->imageWidth = '';
      return;
    }
    if (stripos($link, '://') === false) {
      $link = $this->urlRoot . '/' . ltrim($link, '/');
    }
    $http = new HttpRequest($link);
    $http->setThrowExceptionOnErrorResponse(true);
    $http->request();
    $file = FileSystem::getTempDirectory() . '/' . FileSystem::getBasename($link);
    FileSystem::writeFile($file, $http->getResponseBody());
    $size = @getimagesize($file);
    FileSystem::unlink($file);
    if (empty($size)) {
      throw new LException("Failed to read image size of: :file", array(':file' => $file));
    }
    $this->imageWidth = $size[0];
    $this->imageHeight = $size[1];
    $this->imageLink = $link;
  }

  /**
   * Adds a new feed item by the specified media file path (in the DOCUMENT_ROOT).
   * The item will be initialized with a copy of the matching feed channel data
   * (like author) and data imported from the file.
   * @param string $path
   * @return PodcastFeedItem
   */
  public function & addItem($path) {
    $this->items[$path] = new PodcastFeedItem($path, $this);
    return $this->items[$path];
  }

  /**
   * Renders a podcast channel with surrounding XML/RSS wrapping
   * @return string
   */
  public function compose() {
    // Start RSS
    $xml = '<?xml version="1.0" encoding="UTF-8"?><rss xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:media="http://search.yahoo.com/mrss/" xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" version="2.0">' . "\n";

    // Channel contents
    $xml .= "\n" . '<channel>' . "\n"
            . '<title>' . self::xmlEscape($this->title) . '</title>' . "\n"
            . '<description>' . self::xmlEscape($this->description) . '</description>' . "\n"
            . (empty($this->pageLink) ? '' : ('<link>' . self::xmlEscape($this->pageLink) . '</link>' . "\n"))
            . (empty($this->lastBuildDate) ? '' : ('<lastBuildDate>' . self::xmlEscape($this->lastBuildDate) . '</lastBuildDate>' . "\n"))
            . (empty($this->language) ? '' : ('<language>' . self::xmlEscape($this->language) . '</language>' . "\n"))
            . ('<sy:updatePeriod>hourly</sy:updatePeriod>')
            . ('<sy:updateFrequency>1</sy:updateFrequency>')
            . ('<generator>swlib PodcastFeed.class</generator>')
            . (empty($this->copyright) ? '' : ('<copyright>' . self::xmlEscape($this->copyright) . '</copyright>' . "\n"))
            . (empty($this->authorEmail) ? '' : ('<managingEditor>' . self::xmlEscape($this->authorEmail) . '</managingEditor>' . "\n"))
            . (empty($this->authorEmail) ? '' : ('<webMaster>' . self::xmlEscape($this->authorEmail) . '</webMaster>' . "\n"))
            . ('<ttl>' . self::xmlEscape($this->updatePeriodTtlInS) . '</ttl>' . "\n")
            . (empty($this->imageLink) ? '' : ('<image><url>' . self::xmlEscape($this->imageLink)
                    . '</url><title>' . self::xmlEscape($this->title) . '</title>'
                    . '<link>' . self::xmlEscape($this->pageLink) . '</link><width>' . self::xmlEscape($this->imageWidth) . '</width>'
                    . '<height>' . self::xmlEscape($this->imageHeight) . '</height></image>' . "\n"))
            . '<itunes:subtitle>' . self::xmlEscape($this->description) . '</itunes:subtitle>' . "\n"
            . '<itunes:summary>' . self::xmlEscape($this->description) . '</itunes:summary>' . "\n"
            . '<itunes:keywords>' . self::xmlEscape($this->keywords) . '</itunes:keywords>' . "\n"
            . '<itunes:author />' . "\n"
            . '<itunes:block>' . (empty($this->block) || $this->block == 'no' || $this->block == 'false' ? 'no' : 'yes') . '</itunes:block>' . "\n"
            . '<itunes:explicit>' . (empty($this->explicit) || $this->explicit == 'no' || $this->explicit == 'false' ? 'no' : 'yes') . '</itunes:explicit>' . "\n"
            . '<itunes:image href="' . self::xmlEscapeAttribute($this->imageLink) . '" />' . "\n"
            . '<itunes:owner><itunes:email>' . self::xmlEscape($this->authorEmail) . '</itunes:email></itunes:owner>' . "\n"
            . '<itunes:category text="' . self::xmlEscapeAttribute($this->category) . '" />' . "\n"
    ;

    foreach ($this->items as $k => $v) {
      $xml .= $v->compose();
    }

    $xml .= "\n" . '</channel>' . "\n";
    $xml .= '</rss>' . "\n";
    return $xml;
  }

  /**
   * String conversion function (identical to compose())
   * @return string
   */
  public function __toString() {
    return $this->compose();
  }

  /**
   *
   * @param string $xml_string
   * @return PodcastFeed
   */
  public static function parse($xml_string) {
    $channel = Xml::fromXmlString($xml_string)->toAssocArray();
    if(!isset($channel['rss']) || !isset($channel['rss']['channel'])) {
      throw new LException('The XML string is not a valid podcast feed (tag "rss" or "rss/channel" is missing.');
    }

    $itunes_ns = '';
    foreach($channel['rss'] as $k => $v) {
      if(is_scalar($v) && stripos($v, 'www.itunes.com') !== false) {
        $itunes_ns = trim(str_ireplace('xmlns:', '', $k));
        Tracer::trace("Found iTunes XML NS: $itunes_ns");
        break;
      }
    }

//    Tracer::trace_r($channel, 'Parsed XML feed');

    $channel = $channel['rss']['channel'];

    $feed = new self('');
    $feed->title = trim(isset($channel['title']) ? trim($channel['title']) : '', "\n\r\t ");
    $feed->description = trim(isset($channel['description']) ? trim($channel['description']) : '', "\n\r\t ");
    $feed->author = trim(isset($channel['managingeditor']) ? trim($channel['managingeditor']) : '');
    $feed->authorEmail = trim(isset($channel['webmaster']) ? trim($channel['webmaster']) : '');
    $feed->language = strtolower(trim(isset($channel['language']) ? trim($channel['language']) : ''));
    $feed->lastBuildDate = trim(isset($channel['lastbuilddate']) ? trim($channel['lastbuilddate']) : '');
    $feed->pageLink = trim(isset($channel['link']) ? trim($channel['link']) : '');
    $feed->updatePeriodTtlInS = intval(isset($channel['ttl']) ? trim($channel['ttl']) : $feed->updatePeriodTtlInS);
    $feed->copyright = trim(isset($channel['copyright']) ? trim($channel['copyright']) : '');

    if(isset($channel['image']) && isset($channel['image']['link']) && isset($channel['image']['width']) && isset($channel['image']['height'])) {
      $feed->imageLink = $channel['image']['link'];
      $feed->imageWidth = intval($channel['image']['width']);
      $feed->imageHeight = intval($channel['image']['height']);
    }

    $feed->category = isset($channel["$itunes_ns:category"]) && isset($channel["$itunes_ns:category"]['text']) ? trim($channel["$itunes_ns:category"]['text']) : 'Podcast';
    $feed->explicit = isset($channel["$itunes_ns:explicit"]) ? trim($channel["$itunes_ns:explicit"]) : '';
    $feed->keywords = isset($channel["$itunes_ns:keywords"]) ? trim($channel["$itunes_ns:keywords"]) : '';
    $feed->block = isset($channel["$itunes_ns:block"]) ? trim($channel["$itunes_ns:block"]) : false;

    // Some corrections
    $feed->block = !empty($feed->block) && (@intval($feed->block) != 0 || in_array(strtolower($feed->block), array('yes', 'true')));
    $feed->explicit = !empty($feed->explicit) && (@intval($feed->explicit) != 0 || in_array(strtolower($feed->explicit), array('yes', 'true')));

    // Items
    if(isset($channel['item']) && is_array($channel['item'])) {
      foreach($channel['item'] as $item) {
        if(isset($item['enclosure']) && isset($item['enclosure']['url'])) {

          $url = trim(isset($item['enclosure']['url']) ? $item['enclosure']['url'] : '');
          $feed_item = $feed->addItem($url);
          $feed_item->fileUrl = trim(isset($item['enclosure']['url']) ? $item['enclosure']['url'] : '');
          $feed_item->fileLength = trim(isset($item['enclosure']['length']) ? $item['enclosure']['length'] : '');
          $feed_item->fileMimeType = strtolower(trim(isset($item['enclosure']['type']) ? $item['enclosure']['type'] : ''));
          $feed_item->parentPodcast = $feed;
          $feed_item->title = trim(isset($item['title']) ? trim($item['title']) : '', "\n\r\t ");;
          $feed_item->description = trim(isset($item['description']) ? trim($item['description']) : '', "\n\r\t ");;
          $feed_item->pageLink = trim(isset($item['link']) ? trim($item['link']) : '');
          $feed_item->commentsPageLink = trim(isset($item['comments']) ? trim($item['comments']) : '');
          $feed_item->datePublished = trim(isset($item['pubdate']) ? strtotime(trim($item['pubdate'])) : 0);
          $feed_item->author = isset($item["$itunes_ns:author"]) ? trim($item["$itunes_ns:author"]) : '';
          $feed_item->category = isset($item['category']) ? trim($item['category']) : '';
          //$item->authorEmail = '';
          $feed_item->duration = isset($item["$itunes_ns:duration"]) ? trim($item["$itunes_ns:duration"]) : '';
          $feed_item->keywords = isset($item["$itunes_ns:keywords"]) ? trim($item["$itunes_ns:keywords"]) : '';
          $feed_item->subtitle = isset($item["$itunes_ns:subtitle"]) ? trim($item["$itunes_ns:subtitle"]) : '';
          $feed_item->explicit = isset($item["$itunes_ns:explicit"]) ? trim($item["$itunes_ns:explicit"]) : '';
          $feed_item->block = isset($item["$itunes_ns:block"]) ? trim($item["$itunes_ns:block"]) : false;
          // Some corrections
          $feed_item->block = !empty($feed_item->block) && (@intval($feed_item->block) != 0 || in_array(strtolower($feed_item->block), array('yes', 'true')));
          $feed_item->explicit = !empty($feed_item->explicit) && (@intval($feed_item->explicit) != 0 || in_array(strtolower($feed_item->explicit), array('yes', 'true')));
        }
      }
    }
    return $feed;
  }

}
