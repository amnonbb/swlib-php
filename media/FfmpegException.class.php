<?php

/**
 * FFmpeg exceptions
 * and meta information.
 *
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2007-2012
 * @license GPL
 * @version 1.0
 */

namespace sw;

class FfmpegException extends LException {
  
}
