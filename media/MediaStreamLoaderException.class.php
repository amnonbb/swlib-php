<?php

/**
 * Exceptions for MIMMS media stream downloader class
 *
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2007-2012
 * @license GPL
 * @version 1.0
 */
namespace sw;

class MediaStreamLoaderException extends LException {
  
}
