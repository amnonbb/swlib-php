<?php

class ffmpeg_copy extends sw\FfmpegFormula {

  public function getDescription() {
    return "Does not convert any streams, copies instead.";
  }

  public function getOutputFileExtension() {
    return '';
  }

  public function getArguments() {
    return array(
      array('-vcodec' => 'copy'), 
      array('-acodec' => 'copy')
    );
  }
}
