<?php

class ffmpeg_mp4_h264_aac extends sw\FfmpegFormula {

  public function getDescription() {
    return "mp4 container with H.264/AAC streams";
  }

  public function getOutputFileExtension() {
    return 'mp4';
  }

  public function getArguments() {
    $args = array();

    foreach ($this->inputFileInfo['streams'] as $v) {
      if ($v['type'] == 'video') {
        if ($v['codec'] == 'h264') {
          $args[] = array('-vcodec' => 'copy');
        } else {
          $args[] = array('-vcodec' => 'libx264');
          $args[] = array('-preset' => 'slow');
          $args[] = array('-crf' => 18);
        }
        break;
      }
    }

    foreach ($this->inputFileInfo['streams'] as $v) {
      if ($v['type'] == 'audio') {
        if ($v['codec'] == 'aac') {
          $args[] = array('-acodec' => 'copy');
        } else {
          $args[] = array('-acodec' => 'libfaac');
          $args[] = array('-aq' => 100);
        }
        break;
      }
    }

    return $args;
  }

}
