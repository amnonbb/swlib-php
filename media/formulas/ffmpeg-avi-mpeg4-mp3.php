<?php


class ffmpeg_avi_mpeg4_mp3 extends sw\FfmpegFormula {

  public function getDescription() {
    return "avi container with mpeg4/MP3 streams";
  }

  public function getOutputFileExtension() {
    return 'avi';
  }

  public function getArguments() {
    $args = array();
    foreach ($this->inputFileInfo['streams'] as $v) {
      if ($v['type'] == 'video') {
        if ($v['codec'] == 'mpeg4') {
          $args[] = array('-vcodec' => 'copy');
        } else {
          $args[] = array('-vcodec' => 'mpeg4');
          $args[] = array('-b' => '1600000');
//        $args[] = array('-g' => 300);
        }
        break;
      }
    }

    foreach ($this->inputFileInfo['streams'] as $v) {
      if ($v['type'] == 'audio') {
        if ($v['codec'] == 'mp3') {
          $args[] = array('-acodec' => 'copy');
        } else {
          $args[] = array('-acodec' => 'libmp3lame');
          $args[] = array('-ab' => 192000);
        }
        break;
      }
    }
    return $args;
  }

}
