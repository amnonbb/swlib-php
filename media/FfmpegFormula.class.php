<?php

/**
 * FFmpeg conversion formula base class
 *
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2007-2012
 * @license GPL
 * @version 1.0
 */
namespace sw;

require_once('FfmpegException.class.php');

abstract class FfmpegFormula {

  /**
   * Contains the file info of the input file
   * @var array
   */
  protected $inputFileInfo = array();

  /**
   * The arguments that the user passed to the transcoding function
   * @var array
   */
  protected $userArgs = array();

  /**
   * Supported formats
   * @var array
   */
  protected $formats = array();

  /**
   * Returns what it converts to
   * @rerurn string
   */
  public abstract function getDescription();

  /**
   * Returns which extension the container has
   */
  public abstract function getOutputFileExtension();

  /**
   * Returns the arguments for the conversion excluding the '-i' paramter
   * @return array
   */
  public abstract function getArguments();

  /**
   * Constructor
   * @param array $inputFileInfo
   * @param array $userArgs
   */
  public final function __construct($inputFileInfo=array(), $userArgs=array(), $formats=array()) {
    $this->inputFileInfo = $inputFileInfo;
    $this->userArgs = $userArgs;
    $this->formats = $formats;
  }

}
