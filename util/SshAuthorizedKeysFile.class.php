<?php

/**
 * SSH authorized_keys file handling
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2012
 * @license GPL
 * @version 1.0
 */

namespace sw;

class SshAuthorizedKeysFile {

  /**
   * Path to the file
   * @var string
   */
  public $path = "";

  /**
   * Array containing associative sub arrays, each with a SshKey object, options,
   * and ok/error fields.
   * array(
   *  array(
   *    'key'   => SshKey object or null if the line is invalid
   *    'options' => array(
   *      'no-tty' => true/false      <<< flag indicated using boolean
   *      'from' => "192.168.0.0/24"  <<< key-value has a non boolean value
   *    )
   *    'error' => Exception message if SshKey instantiation failed (line invalid)
   *  ),
   *  array(
   *    Parsed 2nd line of the file
   *  )
   * )
   * @var array
   */
  public $keys = array();


  /**
   * Constructor
   * @param string $file
   */
  public function __construct($file="") {
    if(!empty($file)) {
      $this->load($file);
    }
  }

  /**
   * Parses the contents of a auhorized_key file and assigns the results to
   * the instance variables.
   * @param string $authorizedKeysText
   * @param string $path = null
   */
  public function assign($authorizedKeysText, $path=null) {
    $this->keys = array();
    if($path !== null) $this->path = "";
    $keys = explode("\n", $authorizedKeysText);
    foreach($keys as $k => $v) {
      $v = trim($v, "\r\t ");
      if(!empty($v) && strpos($v, "#") !== 0) {
        $v = explode(" ", $v, 2);
        $keyok = true;
        $li = array('key' => "", 'options' => array());
        if(strpos($v[0], "ssh-") === 0) {
          $li['key'] = implode(" ", $v);
        } else if(strpos($v[1], "ssh-") === 0) {
          $li['key'] = $v[1];
          $opt = array();
          $vstrOpen = false;
          $s = "";
          foreach(str_split($v[0]) as $ch) {
            if($ch == '"' && (strrpos($s, "\\") !== strlen($s)-1 || strrpos($s, "\\\\") === strlen($s)-2) ) {
              $vstrOpen = !$vstrOpen;
              $s .= $ch;
            } else if(!$vstrOpen && $ch == ',') {
              $opt[] = $s;
              $s = "";
            } else {
              $s .= $ch;
            }
          }
          if(strlen($s) > 0) {
            $opt[] = $s;
          }
          foreach($opt as $v) {
            if(strpos($v, "=") !== false) {
              $v = explode("=", $v, 2);
              $li['options'][strtolower($v[0])] = trim($v[1], "\" ");
            } else {
              $li['options'][strtolower($v)] = true;
            }
          }
        } else {
          $keyok = false;
          $li['error'] = "Line format invalid";
        }
        if($keyok) {
          try {
            $o = new SshKey($li['key']);
            $li['key'] = $o;
          } catch(\Exception $e) {
            $li['error'] = $e->getMessage();
            $li['key'] = null;
          }
        }
        $keys[$k] = $li;
      } else {
        $keys[$k] = null;
      }
    }
    $this->keys = array_values(array_filter($keys));
    return $this;
  }

  /**
   * Loads a authorized_keys file.
   * @param string $path
   * @return \sw\SshAuthorizedKeysFile
   * @throws \Exception
   */
  public function load($path) {
    if(is_dir($path) && is_file(rtrim($path, '/') . "/authorized_keys")) {
      $path = rtrim($path, '/') . "/authorized_keys";
    }
    if(!is_file($path)) {
      throw new \Exception("authorized_keys file does not exist: '$path'");
    } else if(!is_readable($path)) {
      throw new \Exception("authorized_keys file is not readable: '$path'");
    } else if(($content = @file_get_contents($path)) === false) {
      throw new \Exception("Failed to read authorized_keys file: '$path'");
    }
    $this->path = $path;
    $this->assign($content);
    return $this;
  }

  /**
   * Saves an authorized_keys file. If no file is specified, the file from
   * where the instance data were loaded from is used as file to save to.
   * Means $this->load($path) sets the default path to save back to.
   * @param string $path = null
   */
  public function save($path=null) {
    if(empty($path)) {
      if(empty($this->path)) {
        throw new \Exception("No path given to save authorized_keys file to: $path");
      }
      $path = $this->path;
    }
    $content = array();
    foreach($this->keys as $key) {
      if(!empty($key['key'])) {
        $options = array();
        foreach($key['options'] as $k => $v) {
          if($v === true) {
            $options[] = trim($k);
          } else if($v !== false) {
            $v = trim(str_replace(array("\\", "\""), array("\\\\","\\\""), $v));
            $options[] = trim($k) . "=\"$v\"";
          }
        }
        $options = implode(",", $options);
        $content[] = trim($options . " " . $key['key']->getPublicKey());
      }
    }
    if(!@file_put_contents($path, implode("\n", $content))) {
      if(is_file($path) && !is_writable($path)) {
        throw new \Exception("The file to write authorized keys to is not writable: $path");
      } else if(is_dir($path)) {
        throw new \Exception("The directory to write is a directory, you need to explicitly write /.../dir/authorized_keys: $path");
      } else if(!is_dir(dirname($path))) {
        throw new \Exception("The parent directory of the authorised keys file does not exist: $path");
      }
    }
    return $this;
  }

  /**
   * Adds a public key to the key list
   * @param \sw\SshKey $publicKey
   * @param array $options
   * @param bool $replaceExistingDefinedByUserAtHost
   * @return \sw\SshAuthorizedKeysFile
   * @throws \Exception
   */
  public function addPublicKey($publicKey, $options=array(), $replaceExistingDefinedByUserAtHost=false) {
    if(empty($publicKey)) {
      throw new \Exception("No key specified to attach.");
    } else if(!is_array($options)) {
      throw new \Exception("Options attached to an added public key must be given as associative array.");
    } else if(!($publicKey instanceof SshKey)) {
      throw new \Exception("Public key to add must be an SshKey object.");
    } else if($publicKey->getPublicKey() == "") {
      throw new \Exception("SshKey object to add does not contain a public key.");
    }
    if($replaceExistingDefinedByUserAtHost) {
      if($publicKey->getUser() != "" && $publicKey->getHost() != "") {
        $this->removePublicKey($publicKey->getUser(), $publicKey->getHost());
      }
    }
    $this->keys[] = array(
      'key' => $publicKey,
      'options' => $options
    );
    return $this;
  }

  /**
   * Removes a key (all found occurances) defined by user and host. Requires
   * that the comment of the corresponding lines contain the comment user@host.
   * @param string $user
   * @param string $host
   * @return \sw\SshAuthorizedKeysFile
   */
  public function removePublicKey($user, $host='') {
    $comment = trim("$user@$host", "@ ");
    foreach($this->keys as $i => $key) {
      if($key['key']->getComment() == $comment) {
        $this->keys[$i] = null;
      }
    }
    $this->keys = array_filter($this->keys);
    return $this;
  }

}
