<?
/**
 * SSH key generation, check, load and save
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2012
 * @license GPL
 * @version 1.0
 */

namespace sw;


class SshKey {

  public static $userNameRules = "Only \"a-z\", \"0-9\", \".\", \"-\", no spaces, at least 2 characters, max 10 characters, all lower case";
  public static $PasswordRules = "At least 6 characters, must contain letters and numbers/special chars";

  public $publicKey = "";
  public $privateKey = "";
  public $size = 0;
  public $type = "";
  public $comment = "";

  /**
   * Generates a private/public key pair and returns the SshKey container object.
   * @param string $user
   * @param string $pass
   * @param string $host=""
   * @param int $bits=4096
   * @return \sw\SshKey
   * @throws \Exception
   */
  public static function generate($user, $pass, $host="", $bits=4096) {
    $user = trim($user);
    $host = trim($host);
    $file = tempnam(sys_get_temp_dir(), "ssh_keygen_tmp");
    @unlink($file);

    // Check user and pass
    if(!is_numeric($bits) || intval($bits) < 1024 || log($bits,2) != intval(log($bits,2))) {
      throw new \Exception("Size of key is not numeric, not a '2^X', value or smaller than 1024");
    } else if(strtolower($user) != $user) {
      throw new \Exception("User name does not fit the naming rules (not lower case)");
    } else if(strlen($user) < 2) {
      throw new \Exception("User name does not fit the naming rules (at least 2 characters)");
    } else if(strlen($user) > 50) {
      throw new \Exception("User name does not fit the naming rules (max 50 characters)");
    } else if(str_replace(array("\n\r\t "), "", $user) !== $user) {
      throw new \Exception("User name does not fit the naming rules (no spaces allowed)");
    } else if(preg_replace('/[a-z0-9\.\-]/','', $user) !== '') {
      throw new \Exception("User name does not fit the naming rules (invalid character)");
    } else if(strlen($pass) < 6) {
      throw new \Exception("Pass phrase not long enough (at least 6 characters)");
    } else if(is_numeric($pass)) {
      throw new \Exception("Pass phrase not strong enough (cannot consist only of digits)");
    } else if(strlen(preg_replace('/[a-zA-Z]/', '', $pass)) == 0) {
      throw new \Exception("Pass phrase not strong enough (cannot consist only of letters)");
    } else if(strlen($host) > 0 && strlen(preg_replace('/[\w]/', '', $host)) > 0) {
      throw new \Exception("Host name is not alphanumeric");
    }

    @exec("which sh", $rt, $rv); $rt=trim(implode("\n", $rt), "\n\r\t ");
    if($rv != 0 || empty($rt)) {
      throw new \Exception("No UNIX like operating system.");
    }
    @exec("which ssh-keygen", $rt, $rv); $rt=trim(implode("\n", $rt), "\n\r\t ");
    if($rv != 0 || empty($rt)) {
      throw new \Exception("Executable ssh-keygen not found.");
    }

    @exec("which timeout", $rt, $rv); $rt=trim(implode("\n", $rt), "\n\r\t ");
    if(!empty($rt)) {
      $timelimit = "timeout -k 11 10";
    } else {
      @exec("which timelimit", $rt, $rv); $rt=trim(implode("\n", $rt), "\n\r\t ");
      if(!empty($rt)) {
        $timelimit = "timelimit -q -T 10";
      } else {
        $timelimit = "";
      }
    }
    $return = new self();
    $shost = strtolower($host);
    $comment = trim("$user@$shost", " @");
    $pass = @escapeshellarg($pass);
    $scomment = @escapeshellarg($comment);
    @exec("$timelimit ssh-keygen -q -b $bits -t rsa -N $pass -C $scomment -f " . @escapeshellarg($file), $rt, $rv);
    $rt=trim(implode("\n", $rt), "\n\r\t ");
    $return->privateKey = trim(@file_get_contents($file), "\n\r\t ");
    $return->publicKey = trim(@file_get_contents("$file.pub"), "\n\r\t ");
    $return->type = 'rsa';
    $return->size = $bits;
    $return->comment = $comment;
    if(!empty($rt)) $return->sshkeygenStdOut = $rt;
    @unlink($file);
    @unlink("$file.pub");

    if($rv != 0) {
      throw new \Exception("Error generating the key pair: $rt");
    } else if(empty($return->privateKey) || empty($return->publicKey)) {
      throw new \Exception("Key was not generated (private/public output keyfiles not found)");
    }
    return $return;
  }

  /**
   * Constructor
   * @param string $publicKeyString
   * @param string $privateKeyString
   */
  public function __construct($publicKeyString="", $privateKeyString="") {
    $this->assign($publicKeyString, $privateKeyString);
  }

  /**
   * Returns the private key file content
   * @return string
   */
  public function getPrivateKey() {
    return $this->privateKey;
  }

  /**
   * Sets the private key file content
   * @param string $fileContentText
   * @return \sw\SshKey
   */
  public function setPrivateKey($fileContentText) {
    $this->assign(null, $fileContentText);
    return $this;
  }

  /**
   * Returns the public key file content (inline format)
   * @return string
   */
  public function getPublicKey() {
    return $this->publicKey;
  }

  /**
   * Sets the text of the public key, which can be inline or full format.
   * @param string $fileContentText
   * @return \sw\SshKey
   */
  public function setPublicKey($fileContentText) {
    $this->assign($fileContentText);
    return $this;
  }

  /**
   * Returns the size of the key in bits
   * @return int
   */
  public function getKeySize() {
    return $this->size;
  }

  /**
   * Returns the comment of the public key file
   * @return type
   */
  public function getComment() {
    return $this->comment;
  }

  /**
   * Sets the comment of the public key
   * @param string $comment
   * @return \sw\SshKey
   */
  public function setComment($comment) {
    $this->comment = str_replace(array("\n","\r"), "", $comment);
    return $this;
  }

  /**
   * Returns the type of encryption (rsa or dsa)
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Returns the user derived from the comment in the public key file
   * @return string
   */
  public function getUser() {
    return strpos($this->comment, "@") !== false ? @reset(@explode("@", $this->comment)) : "";
  }

  /**
   * Returns the host derived from the comment in the public key file
   * @return string
   */
  public function getHost() {
    return strpos($this->comment, "@") !== false ? @end(@explode("@", $this->comment)) : "";
  }


  /**
   * Checks and assigns a public key, optionally a corresponding private key.
   * @param string $publicKeyString
   * @param string $privateKeyString
   * @throws \Exception
   */
  public function assign($publicKeyString="", $privateKeyString="") {
    $this->type = "";
    $this->privateKey = "";
    $this->publicKey = "";
    $this->comment = "";
    $this->size = 0;

    /// PUBLIC KEY
    if(!empty($publicKeyString)) {
      $key = explode("\n", trim($publicKeyString, "\n\r\t "));
      if(count($key) == 1) {
        // Short format
        $key = explode(" ", $key[0]);
        if(count($key) < 2) {
          throw new \Exception("Public ssh key is not formatted correctly.");
        } else if(strpos($key[0], "ssh-") !== 0) {
          throw new \Exception("Public ssh key does not start with 'ssh-'.");
        } else if(strpos($key[0], "rsa") === false && strpos($key[0], "ds") === false) {
          throw new \Exception("Public ssh key type is not 'rsa' nor 'dsa'.");
        } else if(($binkey = base64_decode($key[1], true)) === false) {
          throw new \Exception("Public ssh key string is no valid BASE64 text.");
        }
        $this->publicKey = $publicKeyString;
        array_shift($key);
        $base64key = array_shift($key);
        $this->comment = trim(implode(" ", $key));
      } else {
        // Long format
        if(stripos($key[0], "BEGIN SSH") === false || stripos($key[0], "PUBLIC") === false) {
          throw new \Exception("Public ssh key is not valid (incorrect start line)");
        } else if(stripos(end($key), "END SSH") === false) {
          throw new \Exception("Public ssh key is not valid (incorrect end line)");
        }
        array_pop($key);
        array_shift($key);
        $comments = array();
        while(!empty($key) && strpos($key[0],":") !== false) {
          $v = explode(":", array_shift($key), 2);
          $v[0] = trim(strtolower($v[0]));
          $comments[$v[0]] = trim($v[1]);
          if($v[0] == 'comment') {
          }
        }
        $base64key = implode("", $key);
        if(($binkey = base64_decode($base64key, true)) === false) {
          throw new \Exception("Public ssh key is not valid (base64 key text invalid)");
        }

        $this->debug = $comments;
        foreach($comments as $k => $v) {
          if(strpos($k, "comment") === 0) {
            $this->comment = trim($v);
            break;
          }
        }
      }

      // Key type
      $sz = @reset(unpack("N", $binkey));
      $binkey = substr($binkey, 4);
      $sv = @reset(unpack("A{$sz}", $binkey));
      $binkey = substr($binkey, $sz);
      if(strpos($sv, 'ssh-') !== 0) {
        throw new \Exception("Public ssh key is not valid (corrupt binary key)");
      }
      if(strpos($sv, "-rsa") !== false) {
        $this->type = 'rsa';
        $binkey = substr($binkey, 8);
        $sv = @reset(unpack("N", $binkey))/32-8;
        $binkey = substr($binkey, 4);
        if($sv != strlen($binkey)*8) {
          throw new \Exception("Public ssh key is not valid (corrupt binary key, length declaration does not match the real length)");
        }
        $this->size = $sv;
      } else if(strpos($sv, "-ds") !== false) {
        $this->type = 'dsa';
        $this->size = @reset(unpack("N", $binkey))*8-8;
      } else {
        throw new \Exception("Public ssh key is not valid (binary key not RSA nor DSA)");
      }
      if(empty($this->publicKey)) {
        $this->publicKey = "ssh-" . trim($this->type) . " " . trim($base64key) . " ". trim($this->comment);
      }
    }

    /// PRIVATE KEY
    if(!empty($privateKeyString)) {
      $prv = explode("\n", str_replace("\r", "", $privateKeyString));
      array_pop($prv);
      array_shift($prv);
      while(!empty($prv) && strpos($prv[0], ":") !== false) array_shift($prv);
      if(base64_decode(trim(implode("", $prv),"\n\r\t "), true) === false) {
        throw new \Exception("Private key corrupt (invalid base64 text)");
      }
      $this->privateKey = $privateKeyString;
    }
  }

  /**
   * Load
   * @param string $publicKeyFile
   * @param string $privateKeyFile
   * @return \sw\SshKey
   * @throws \Exception
   */
  public static function load($publicKeyFile="", $privateKeyFile="") {
    $key = new self();
    $pub = $prv = "";
    if(!empty($publicKeyFile)) {
      $pub = @file_get_contents($publicKeyFile);
      if(empty($pub)) {
        if(!is_file($publicKeyFile) || !is_readable($publicKeyFile)) {
          throw new \Exception("Public ssh keyfile not readable: '$publicKeyFile'");
        } else {
          throw new \Exception("Public ssh keyfile is empty: '$publicKeyFile'");
        }
      }
    }
    if(!empty($privateKeyFile)) {
      $prv = @file_get_contents($privateKeyFile);
      if(empty($prv)) {
        if(!is_file($privateKeyFile) || !is_readable($privateKeyFile)) {
          throw new \Exception("Private ssh keyfile not readable: '$privateKeyFile'");
        } else {
          throw new \Exception("Private ssh keyfile is empty: '$privateKeyFile'");
        }
      }
    }
    $key->assign($pub, $prv);
    return $key;
  }

  /**
   * Save public key or private key to file
   * @param string $publicKeyFile
   * @param string $privateKeyFile
   * @throws \Exception
   */
  public function save($publicKeyFile="", $privateKeyFile="") {
    if($publicKeyFile != "") {
      if(empty($this->publicKey)) {
        throw new \Exception("No public key to save");
      } else if(!file_put_contents($publicKeyFile, $this->publicKey)) {
        throw new \Exception("Failed to save public key");
      }
    }
    if($privateKeyFile != "") {
      if(empty($this->privateKey)) {
        throw new \Exception("No private key to save");
      } else if(!file_put_contents($privateKeyFile, $this->privateKey)) {
        throw new \Exception("Failed to save private key");
      }
    }
  }

}
