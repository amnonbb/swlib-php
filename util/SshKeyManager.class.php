<?php

/**
 * SSH key manager class
 */

namespace sw;

class SshKeyManager {

  /**
   * Class configuration
   * @var array
   */
  private static $config = array(
    'output-key-dir' => '',
    'user-registry-file' => '',
    'key-bits' => 1024,
    'key-host' => "localhost"
  );

  /**
   * Assoc array containineg all users. Projects the user-registry-file.
   * @var array
   */
  protected static $users = array();

  /**
   * Indicates unsaved changes are made, causes autosave in instance
   * destructor.
   * @var bool
   */
  protected static $changed = false;

  /**
   * Singleton instance
   * @var self
   */
  protected static $instance = null;

  /**
   * Class configuration
   * @param array $config
   * @throws \Exception
   */
  public static function init($config=array()) {
    self::$users = array();

    if(is_array($config)) {
      self::$config = array_merge(self::$config, $config);
    }
    if(!is_dir(self::$config['output-key-dir'])) {
      throw new \Exception("SSH key registry output directory not existent");
    } else if(!is_writable(self::$config['output-key-dir'])) {
      throw new \Exception("SSH key registry output directory not writable");
    }
    $data = @file_get_contents(self::$config['user-registry-file']);
    if($data === false) {
      if(empty(self::$config['user-registry-file'])) {
        throw new \Exception("SSH key registry is not specified");
      } else if(is_file(self::$config['user-registry-file'])) {
        throw new \Exception("SSH key registry is not readable");
      }
    }
    $data = @json_decode($data, true);
    if($data === false) {
      throw new \Exception("SSH key registry is corrupt");
    }
    if(!empty($data)) {
      self::$users = $data;
    }

    if(!self::$instance) {
      self::$instance = new self();
    }
  }


  /**
   * Singleton destructor. Causes autosaving changes.
   */
  public function __destruct() {
    if(self::$changed) {
      try {
        self::save();
      } catch(\Exception $e) {
        // LOG
      }
    }
  }

  /**
   * Saves changes and resets then 'changed' flag.
   * @return void
   * @throws \Exception
   */
  public static function save() {
    if(!self::$changed) return;
    self::update();
    if(!@file_put_contents(self::$config['user-registry-file'] , json_encode(self::$users, JSON_FORCE_OBJECT) . "\n\n")) {
      throw new \Exception("Failed to save ssh registry.");
    }
    self::$changed = false;
  }

  /**
   * Updates the registry (without writing changes to the user-registry-file),
   * creates/deletes the SSH public key files in the output directory corresponding
   * to the existance and enabled flag in the registry file.
   * Returns true on success, or an array containing error messages.
   * @return mixed
   */
  protected static  function update() {
    $errors = array();
    if(!is_dir(self::$config['output-key-dir']) || !is_writable(self::$config['output-key-dir'])) {
      $errors[] = "Ssh key file directory not writable.";
    } else {
      foreach(self::$users as $uid => $usr) {
        try {
          if(trim($uid) == '' || preg_replace("/[^\w\d]/", '', $uid) == '') {
            unset(self::$users[$uid]);
          } else {
            if(isset($usr['generated'])) {
              unset(self::$users[$uid]['generated']);
            }
            $pubfile = self::$config['output-key-dir'] . "/$uid.pub";
            if(!isset($usr['enabled']) || !$usr['enabled'] || !isset($usr['pubkey']) || empty($usr['pubkey'])) {
              if(is_file($pubfile) && !@unlink($pubfile)) {
                $errors[] = "Failed to delete ssh key file $pubfile";
              }
            } else if(!is_file($pubfile) || (isset($usr['generated']))) {
              if(!@file_put_contents($pubfile, $usr['pubkey'])) {
                $errors[] = "Failed to save ssh key file $pubfile";
              }
            }
          }
        } catch(\Exception $e) {
          $errors[] = "$e";
        }
      }
      foreach(glob(self::$config['output-key-dir'] . "/*.pub") as $f) {
        if(!isset(self::$users[trim(str_replace(".pub", "", basename($f)))])) {
          // debug("UNLINK $f (" . trim(str_replace(".pub", "", basename($f))) . ")");
          @unlink($f);
        }
      }
    }
    return empty($errors) ? true : implode("\n\n", $errors);
  }

  /**
   * Generates a key pair and adds the user to the registry. Returns true on
   * success or an error message on fail.
   * @param string $user
   * @param string $pass
   * @param string $uid
   * @return mixed
   */
  public static function generate($user, $pass, $uid) {
    try {
      if(!isset(self::$users[$uid])) self::$users[$uid] = array();
      self::$users[$uid]['name'] = $user;
      if(!isset(self::$users[$uid]['enabled'])) self::$users[$uid]['enabled'] = false;
      self::$users[$uid]['generated'] = true;
      self::$users[$uid]['pubkey'] = "";
      self::$users[$uid]['prvkey'] = "";
      self::$changed = true;
      require_once(dirname(__FILE__) . "/SshKey.class.php");
      $ssh = new SshKey();
      try {
        $keys = $ssh->generate(preg_replace("/[\W\s]/", "", str_replace(array(' '), '', strtolower($user))), $pass, self::$config['key-host'], self::$config['key-bits']);
      } catch(\Exception $ex) {
        return "SSH key generator unhappy: " . $ex->getMessage();
      }
      self::$users[$uid]['pubkey'] = $keys->publicKey;
      self::$users[$uid]['prvkey'] = $keys->privateKey;

      if(($r = self::update()) !== true) {
        //debug($r);
      }

    } catch(\Exception $e) {
      return "Error: " . $e->getMessage();
    }
    return true;
  }

  /**
   * Deletes a user from the registry
   * @param string $uid
   */
  public static function delete($uid) {
    if(isset(self::$users[$uid])) {
      unset(self::$users[$uid]);
      self::$changed = true;
    }
  }

  /**
   * Enables access to a user by setting the users 'enabled' flag.
   * @param string $uid
   */
  public static function allow($uid) {
    if(!isset(self::$users[$uid])) self::$users[$uid] = array();
    self::$users[$uid]['enabled'] = true;
    self::$changed = true;    
  }

  /**
   * Disables access to a user by setting the users 'enabled' flag. This does
   * not delete the user, only causes the public key file not being in the
   * output key directory.
   * @param string $uid
   */
  public static function deny($uid) {
    if(isset(self::$users[$uid])) {
      self::$users[$uid]['enabled'] = false;
      self::$changed = true;
    }
  }

  /**
   * Checks if a user is registered and has a key pair. Does NOT return false
   * if the user has a key pair but is disabled.
   * @param string $uid
   * @return bool
   */
  public static function hasKey($uid) {
    if(!isset(self::$users[$uid])) return false;
    $u = &self::$users[$uid];
    if(!isset($u['pubkey'])) return false;
    if(!isset($u['prvkey'])) return false;
    if(empty($u['pubkey'])) return false;
    if(empty($u['prvkey'])) return false;
    return true;
  }

  /**
   * Finds a user by his name. Returns an empty array if no user was found.
   * @param string $name
   * @return array
   */
  public static function find($name) {
    foreach(self::$users as $uid => $user) {
      if(isset($user['name']) && strtolower($user['name']) == strtolower($name)) {
        $user['uid'] = $uid;
        return $user;
      }
    }
    return array();
  }

  /**
   * Generates a PuTTY PPK file from an existing private OpenSSH key file.
   * @param string $name
   * @param string $pass
   * @param string $uid
   */
  public static function generatePuttyPrivateFile($name, $pass='', $uid=null) {
    try {
      @exec('which putt ygen', $rt, $rc);
      $rt = trim(implode("\n", $rt), "\n\r\t ");
      if($rt == '') {
        throw new \Exception("Sorry, can't do this: puttygen is not installed on the server or not found.");
      }
      if($uid !== null) {
        $data = isset(self::$users[$uid]) ? self::$users[$uid] : array();
      } else {
        $data = self::find($name);
        if(isset($data['uid'])) $uid = $data['uid'];
      }
      if(!self::hasKey($uid)) {
        throw new \Exception("Sorry, can't do this: You don't have a key yet that could be converted to PuTTY.");
      }
      $prv_file = @tempnam(sys_get_temp_dir(), 'puttygen_pkey');
      if($prv_file === false) {
        throw new \Exception("Sorry, can't do this: Failed to generate an intermediate file.");
      }
      @chmod($prv_file, 0666);
      $prv_outfile = @tempnam(sys_get_temp_dir(), 'puttygen_pkey_outfile');
      if($prv_outfile === false) {
        throw new \Exception("Sorry, can't do this: Failed to generate an intermediate file.");
      }
      if(!@file_put_contents($prv_file, self::$users[$uid]['prvkey'])) {
        throw new \Exception("Sorry, can't do this: Failed to save private key in an intermediate file.");
      }
      @chmod($prv_outfile, 0666);
      @exec('echo ' . escapeshellarg($pass) . ' | puttygen ' . escapeshellarg($prv_file) . ' -o ' . escapeshellarg($prv_outfile) . ' -q -O private ', $rt, $rc);
      $puttykey = @file_get_contents($prv_outfile);
    } catch(\Exception $e) {
      $ex = $e;
    }
    if(is_file($prv_file)) @unlink($prv_file);
    if(is_file($prv_outfile)) @unlink($prv_outfile);
    if(isset($ex)) {
      throw $ex;
    }
    return $puttykey;
  }


  /**
   * Shows a simple web ui to show his/her stats/access of a user. Expected
   * to be included in a PHP script that checks authentication and passes
   * e.g. $_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'] and a user id
   * from a database (or e.g. $_SERVER['PHP_AUTH_USER'] as $uid).
   * The method expects to handle $_GET and $_POST itself. The invoking PHP
   * script shall shall not interfer.
   *
   * @param string $name
   * @param string $pass=''
   * @param string $uid=null
   */
  public static function showSimpleHtmlUi($name, $pass='', $uid=null) {
    // Arg inputs
    $arg_regenerate_keypair = (isset($_POST['regenerate-keys']) && $_POST['regenerate-keys'] == 'yes');
    $arg_download_public_key = isset($_GET['download-public-key']);
    $arg_download_private_key = isset($_GET['download-private-key']);
    $arg_download_putty_private_key = isset($_GET['download-putty-private-key']);

    $errors = array();
    if($uid !== null) {
      $data = isset(self::$users[$uid]) ? self::$users[$uid] : array();
    } else {
      $data = self::find($name);
      if(isset($data['uid'])) $uid = $data['uid'];
    }

    if(!is_array($data)) {
      $data = array();
    }

    $enabled = isset($data['enabled']) ? $data['enabled'] : false;
    if($arg_download_public_key) {
      header("Content-Type: text/plain;", true);
      if(!empty($data['pubkey'])) print $data['pubkey'] . "\n";
      exit();
    } else if($arg_download_private_key) {
      header("Content-Type: text/plain;", true);
      if(!empty($data['prvkey'])) print $data['prvkey'] . "\n";
      exit();
    } else if($arg_download_putty_private_key) {
      header("Content-Type: text/plain;", true);
      try {
        print self::generatePuttyPrivateFile($name, $pass, $uid);
      } catch(\Exception $e) {
        print $e->getMessage();
      }
      exit();
    }

    // Generate a key automatically if appropriate
    if(!self::hasKey($uid) || $arg_regenerate_keypair) {
      try {
        $gen_return = self::generate($name, $pass, $uid);
        if($gen_return === true) {
          $gen_return = '<span class="ok">A key was just generated for you.</span>';
          $data = self::$users[$uid];
        } else {
          $gen_return = '<span class="nok">' . htmlspecialchars($gen_return) . '</span>';
        }
      } catch(\Exception $e) {
        $gen_return = '<span class="nok">Error generating a key:' . htmlspecialchars($e->getMessage()) . '</span>';
      }
    }

    // Generate html ...
    header("Content-Type: text/html;", true);
    ?><!doctype html><html><head>
      <title>SSH access of <? print htmlspecialchars($name); ?></title>
      <style type="text/css">
        html, body, h1, li, span, a, a:link, a:visited, a:hover {
          font-family: sans-serif;
          font-size: 11pt;
          text-decoration: none;
          color: #222222;
        }

        body {
          min-width: 99%;
          background: #fff url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAEAQMAAACTPww9AAAABlBMVEX///+up58H0gfvAAAAAXRSTlMAQObYZgAAABVJREFUeF4dwAENAAAAQDDRRbc5sgADiACBp7r5nAAAAABJRU5ErkJggg==) repeat bottom;
        }

        h1 {
          font-size: 14pt;
        }
        pre {
          background-color: lightgray;
        }
        .ok {
          color: green;
        }
        .nok {
          color: red;
        }
        form, input {
          display: inline;
        }
        a:link, a:visited{
          color: #333388;
        }
        a:hover {
        }
        li {
          margin-top: 5px;
          margin-left: 0px;
          list-style-type: square;
        }
        ul {
          margin: 0;
          padding-left: 20px;
        }

        pre {
          border-radius: 5px;
          -moz-border-radius: 5px;
          -webkit-border-radius: 5px;
          box-shadow: rgb(170, 170, 170) 0px 0px 3px 0px;
          -webkit-box-shadow: rgb(170, 170, 170) 0px 0px 3px 0px;
          -moz-box-shadow: rgb(170, 170, 170) 0px 0px 3px 0px;
          padding: 5px;
        }

        #sshkey-wrapper {
          display: block;
          border-radius: 5px;
          -moz-border-radius: 5px;
          -webkit-border-radius: 5px;
          box-shadow: rgb(170, 170, 170) 0px 0px 3px 0px;
          -webkit-box-shadow: rgb(170, 170, 170) 0px 0px 3px 0px;
          -moz-box-shadow: rgb(170, 170, 170) 0px 0px 3px 0px;
          width: 700px;
          margin: 20px auto 10px auto;
          padding: 10px;
          background: #fefefe;
        }

      </style>
    </head><body><div id="sshkey-wrapper">
      <h1>Your SSH access</h1>
      <ul>
        <? if(isset($gen_return)) print "<li>$gen_return</li>"; ?>
        <li><? print $data['enabled'] ? "<span class=\"ok\">Your SSH access is enabled</span>" : '<span class="nok">Your SSH access is not enabled</span> (ask admin)'; ?></li>
        <li>Download the <b><a href="<?= $_SERVER['PHP_SELF'] ?>?download-private-key">private</a></b> and
          <b><a href="<?= $_SERVER['PHP_SELF'] ?>?download-public-key">public</a></b> key file on computer
          for later use with a SSH program.</li>
        <li>Use the same password for the keyfile as you use for the web page (like this site)</li>
        <li><b>Your public key <a href="<?= $_SERVER['PHP_SELF'] ?>?download-public-key"> (download)</a>:</b><br/><pre><? print isset($data['pubkey']) ? wordwrap($data['pubkey'], 64, "\n", true) : '<span class="nok">Not set</span>'; ?></pre></li>
        <li><b>Your private key <a href="<?= $_SERVER['PHP_SELF'] ?>?download-private-key"> (download)</a>:</b><br/><pre><? print isset($data['prvkey']) ? $data['prvkey'] : '<span class="nok">Not set</span>'; ?></pre></li>
        <li>You can let the server generate a PuTTY private key file for you by clicking <a href="<?= $_SERVER['PHP_SELF'] ?>?download-putty-private-key">here</a>.</li>
      </ul>
      <br/><br/>
      <span style="font-size:10px; color:gray;">stfwi, MISEC, 2013</span>
    </body></html>
    <?
  }
}
