<?
/**
 * Password check utilities
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2012
 * @license GPL
 * @version 1.0
 */

namespace sw;


class PasswordCheck {

  /**
   *
   * @staticvar array
   */
  public static $config = array(
      'special-chars' => "+-_!@$%&*()[]{};:'?/<>,.~ ",
      'good-length' => 8,
      'bad-length' => 4,
      'good-relative-character-variety' => 0.6,
      'bad-relative-character-variety' => 0.2,
      'good-num-of-possibilities' => 20,
      'bad-num-of-possibilities' => 5,
  );


  /**
   * Returns the strength statistics of a password.
   * @param string $pass
   * @return array
   */
  public static function getStrength($pass) {
    $SPECIALCHARS = self::$config['special-chars'];
    $GOOD_LENGTH = self::$config['good-length'];
    $BAD_LENGTH = self::$config['bad-length'];
    $GOOD_REL_DIFF_CHARS = self::$config['good-relative-character-variety'];
    $BAD_REL_DIFF_CHARS = self::$config['bad-relative-character-variety'];
    $GOOD_POSSIB = self::$config['good-num-of-possibilities'];
    $BAD_POSSIB = self::$config['bad-num-of-possibilities'];

    $return = array('total' => 0, 'length' => 0, 'chars-variety' => 0, 'types-variety' => 0, 'possibilities' => 0);
    if(strlen($pass) == 0) return $return;

    // Length check
    $len = strlen($pass);
    $v = ($len-$BAD_LENGTH) / ($GOOD_LENGTH-$BAD_LENGTH);
    $return['length'] = round($v > 1 ? 1 : ($v < 0 ? 0 : $v), 1);

    // How many different characters are used in relation to the length?
    $chars = array();
    $types = array('num' => 0, 'lower' => 0, 'upper' => 0, 'special' => 0, 'other' => 0);
    $types_count = count($types) > $len ? $len : count($types);

    foreach(str_split($pass) as $v) {
      $chars[$v] = !isset($chars[$v]) ? 1 : $chars[$v]+1;
      if(is_numeric($v)) {
        $types['num']++;
      } else if(ctype_lower($v)) {
        $types['lower']++;
      } else if(ctype_upper($v)) {
        $types['upper']++;
      } elseif(strpos($SPECIALCHARS, $v) !== false) {
        $types['special']++;
      } else {
        $types['other']++;
      }
    }
    $v = (((count($chars)-1)/$len)-$BAD_REL_DIFF_CHARS) / ($GOOD_REL_DIFF_CHARS-$BAD_REL_DIFF_CHARS);
    $return['chars-variety'] = $v > 1 ? 1 : ($v < 0 ? 0 : $v);
    $v = round((count(array_filter($types))+2) / $types_count, 1);
    $return['types-variety'] = $v > 1 ? 1 : ($v < 0 ? 0 : $v);

    // Space
    $char_possibilities = 1
      + ($types['num'] <= 0 ? 0 : 10)
      + ($types['lower'] <= 0 ? 0 : 26)
      + ($types['upper'] <= 0 ? 0 : 26)
      + ($types['special'] <= 0 ? 0 : strlen($SPECIALCHARS)
      + ($types['other'] <= 0 ? 0 : 30)
      );

    $possibilities = round(log($char_possibilities, 10) * $len, 1);        
    $v = ($possibilities-$BAD_POSSIB) / ($GOOD_POSSIB-$BAD_POSSIB);
    $return['possibilities'] = round($v > 1 ? 1 : ($v < 0 ? 0 : $v), 1);
            
    $return['stats'] = array(
        'length' => $len,
        'types' => $types,
        'possibilities-per-character' => $char_possibilities,
        'possibilities-total-10-to-pow-of' => $possibilities
    );

    $return['total'] = round((
        $return['length']
      * $return['chars-variety'] 
      * $return['types-variety']
      * $return['possibilities']
    ), 8);
    
    return $return;
  }
}
