<?php

/**
 * XML handling. Generates XML output from an associative netsted array.
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2006-2010
 * @license GPL
 * @version 1.0
 */

namespace sw;

class XmlConverter {

  /**
   * Transforms an associative array to an XML string. If a root node
   * tag is specified, it is wrapped around the contents.
   * @param array $assocArray
   * @param string $rootNodeTag
   * @return string
   */
  public static function fromAssocArray($assocArray, $rootNodeTag='') {
    if (trim($rootNodeTag) != '') {
      if (!ctype_alnum(str_replace('_', '', $rootNodeTag))) {
        throw new LException('root node tag must be alphanumeric.');
      }
      $xml = new \SimpleXMLElement("<$rootNodeTag></$rootNodeTag>");
    } else if (count($assocArray) == 1 && is_array(reset($assocArray))) {
      $rootNodeTag = key($assocArray);
      $assocArray = reset($assocArray);
    } else {
      throw new LException('Array to convert has no root node and no root node tag defined');
    }

    foreach ($assocArray as $key => $value) {
      if (is_numeric($key)) {
        $key = 'element';
      }
      if (!is_array($value)) {
        $element = $xml->addChild($key, $value);
      } else {
        $element = $xml->addChild($key);
        self::fromAssocArrayR($element, $value);
      }
    }
    return $xml->asXML();
  }

  /**
   * Recursive helper function
   * @param string $xml
   * @param array $assocArray
   */
  private static function fromAssocArrayR($xml, $assocArray) {
    foreach ($assocArray as $key => $value) {
      if (is_numeric($key)) {
        $key = 'element';
      }
      if (!is_array($value)) {
        $element = $xml->addChild($key, $value);
      } else {
        $element = $xml->addChild($key);
        self::fromAssocArrayR($element, $value);
      }
    }
  }

}

?>