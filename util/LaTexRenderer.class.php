<?php

/**
 * Renders LaTeX to a png image. The rendering class has an own cache based on
 * the checkums of the fomulas. If a new formula is entered specified, then the
 * rendering process will run, and create the cache file. The callback
 * onTexFilter() can be overloaded to add an own filter, e.g. to prevent too long
 * texts, shell commands or require HTTP authentication to render formulas.
 * @gpackage de.atwillys.sw.php.swLib
 * @author Stefan Wilhelm
 * @copyright Stefan Wilhelm, 2010
 * @license GPL
 * @version 1.0
 */

namespace sw;

class LaTeXRenderer {

  /**
   * Stores the class configuration
   * @staticvar array
   */
  private static $config = array(
      'latex_bin' => '/usr/texbin/latex',
      'dvips_bin' => '/usr/texbin/dvips',
      'convert_bin' => '/opt/local/bin/convert',
      'cache_dir' => '',
      'cache_prefix' => 'texf_',
      'temp_dir' => '',
      'font_size' => 16,
      'packages' => array(
          'amsmath', 'amsfonts', 'amssymb', 'latexsym', 'color'
      )
  );

  /**
   * Sets and returns the class configuration
   * @param array $config
   * @return array
   */
  public static function config($config=null) {
    if (!empty($config)) {
      if (!is_array($config)) {
        throw new LException('Class ":class" must be configured using an array', array(':class' => __CLASS__));
      } else {
        self::$config = array_merge(self::$config, $config);
      }
    }
    return self::$config;
  }

  /**
   * Overload this function to filter the input TeX sources, or e.g. using
   * HTTP authentication to enable it. You can return a text that renders
   * an error message.
   * @param string $tex
   * @return string
   */
  protected function onTexFilter($tex) {
    return $tex;
  }

  /**
   * Constructor, automatically initializes the undefined configuration
   * if the temp_directory is empty.
   */
  public function __construct() {
    if (empty(self::$config['cache_dir']))
      self::config(array('cache_dir' => './cache'));
    if (empty(self::$config['temp_dir']))
      self::config(array('temp_dir' => '/tmp'));
  }

  /**
   * Render a formula to an image
   * @return string
   */
  public function renderFormulaToImage($tex) {
    return $this->renderToImage("\\documentclass[10pt]{article}\n\$packages\\pagestyle{empty}\n\\begin{document}\n\\begin{displaymath}\n$tex\n\\end{displaymath}\n\\end{document}\n");
  }

  /**
   * Renders a LaTeX source to an image (png) if the md5 checksum of the
   * source text already exists, a cached image representation is returned.
   * The images are located in in the cache directory.
   * @param string $tex
   * @return string
   */
  private function renderToImage($tex) {
    $tex = $this->onTexFilter($tex);
    $fileName = trim(self::$config['cache_prefix'] . md5($tex));
    $cacheFilePath = self::$config['cache_dir'] . "/$fileName.png";

    if (!is_file($cacheFilePath)) {
      $exception = null;
      try {
        $packages = self::$config['packages'];
        foreach ($packages as $key => $package) {
          $packages[$key] = "\usepackage{" . $package . "}";
        }
        $packages = implode("\n", $packages);
        $font_size = self::$config['font_size'];
        $tex = str_replace('$packages', $packages, $tex);
        $current_dir = getcwd();
        $tmpDir = self::$config['temp_dir'];
        $cacheDir = self::$config['cache_dir'];

        chdir($tmpDir);
        file_put_contents("$tmpDir/$fileName.tex", $tex);

        $output = array();
        exec(self::$config['latex_bin'] . " --interaction=nonstopmode $tmpDir/$fileName.tex", $output, $return_var);
        @unlink("$tmpDir/$fileName.tex");
        @unlink("$tmpDir/$fileName.aux");
        @unlink("$tmpDir/$fileName.log");

        if (!is_file("$tmpDir/$fileName.dvi")) {
          $error = '';
          foreach ($output as $line) {
            $line = trim($line);
            if (!empty($line)) {
              if (substr($line, 0, 1) == '!') {
                $error = trim($line, "!. ");
              } else if (!empty($error)) {
                $line = ltrim($line, ' l.0123456789');
                throw new LException("LaTeX: \":error\" in line :line", array(':error' => $error, ':line' => $line));
              }
            }
          }
          if (!empty($error)) {
            throw new LException('LaTeX: :error', array(':error' => $error));
          } else {
            throw new LException('LaTeX did not generate output file');
          }
        }

        $output = array();
        exec(self::$config['dvips_bin'] . " -E $tmpDir/$fileName.dvi -o $tmpDir/$fileName.ps", $output, $return_var);
        @unlink("$tmpDir/$fileName.dvi");
        if (!is_file("$tmpDir/$fileName.ps")) {
          throw new LException('LaTeX/dvips conversion failed: :output', array(':output' => implode("\n", $output)));
        }

        $output = array();
        chmod("$tmpDir/$fileName.ps", 0666);
        exec(escapeshellcmd(self::$config['convert_bin'] . " -define registry:temporary-path=$tmpDir -density 120 $tmpDir/$fileName.ps $tmpDir/$fileName.png"));
        @unlink("$tmpDir/$fileName.ps");
        if (is_file("$tmpDir/$fileName.png")) {
          copy("$tmpDir/$fileName.png", "$cacheDir/$fileName.png");
          @unlink("$tmpDir/$fileName.png");
        } else {
          throw new LException('Conversion from to ps to png failed: :output', array(':output' => implode("\n", $output)));
        }
      } catch (\Exception $e) {
        $exception = $e;
      }

      chdir($current_dir);

      // Rethrow the exception after cleanup and directory restored
      if (!empty($exception)) {
        throw $exception;
      }
    }

    return "$fileName.png";
  }

}
